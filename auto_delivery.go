package main

import (
	"auto_delivery/config"
	"auto_delivery/controller"
	"crypto/tls"
	module_connect "ff_system/connect"
	"ff_system/connect/structExchange"
	"ff_system/function"
	"ff_system/logger"
	"fmt"
	"net"
	"strings"
	"time"
)

func handleConnectionNew(conn net.Conn) {

	defer function.GetDeferPanic()
	defer conn.Close()

	for {
		nws := module_connect.NewWorkServer(conn)
		mes := nws.Read()
		if nws.GetError() != nil {
			break
		}

		if mes.String() == "PING" {
			module_connect.Send([]byte("PONG"), conn)
			continue
		}

		var operationStartTime time.Time
		var operationHash string
		var logging = false || !strings.Contains(mes.String(), "Query\":\"Check") && !strings.Contains(mes.String(), "Query\":\"Select")

		if !logging {
			operationStartTime, operationHash = function.LogStartTimeOperation(mes.String())
		}

		var res = structExchange.WMessage{}

		func() {
			//Парсим данные
			message, err := mes.NewServerActualPartialParsing()
			if err != nil {
				fmt.Print(err)
				logger.Errors.Println(err)
				nws.SendINJSON(structExchange.WMessage{Error: structExchange.Error{Code: structExchange.UnmarshalMessage, Description: err.Error()}})
				return
			}
			//fmt.Print("IP: ", message.RequestInfo.RemoteAddr, " URI: ", message.RequestInfo.RequestURI)

			if logging {
				logger.Messaging.Println("IP: ", message.RequestInfo.RemoteAddr, " URI: ", message.RequestInfo.RequestURI)
				logger.Messaging.Println(fmt.Sprintf("messange %#v", message))
			}
			res.Query = message.Query

			//Глобальный объект для работы
			WORKING := NewWorking()

			//Выполняем действия
			res.Tables, res.Error = WORKING.Action(message)
			if res.Error.Code != 0 {
				logger.Errors.Println(res.Error)
				nws.SendINJSON(res)
				return
			}

			nws.SendINJSON(res)
		}()

		if !logging {
			function.LogEndTimeOperation(operationStartTime, operationHash, res)
		}

	}
	logger.Messaging.Println("[END GORUTIN]")
}

func main() {

	fmt.Println("Start")
	_conf, err := module_connect.GetConfigTlsSertification()
	if err != nil {
		logger.Errors.Println(err)
		panic(err)
	}

	listener, err := tls.Listen("tcp", config.Config.TLS_SERVER.TlsHost+":"+config.Config.TLS_SERVER.TlsPort, &_conf)
	if err != nil {
		logger.Errors.Println(err)
		panic(err)
	}
	defer listener.Close()

	logger.Console.Println("AutoDelivery")
	logger.Console.Println("IP:", config.Config.TLS_SERVER.TlsHost+":"+config.Config.TLS_SERVER.TlsPort)
	logger.Application.Println("AutoDelivery")
	logger.Application.Println("IP:", config.Config.TLS_SERVER.TlsHost+":"+config.Config.TLS_SERVER.TlsPort)

	controller.InitController()

	for {
		conn, err := listener.Accept()
		if err != nil {
			//logger.Errors.Println(err)
			continue
		}
		go handleConnectionNew(conn)
	}
}
