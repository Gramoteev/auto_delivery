package redisConfig

type Data struct {
	Coefficient              float64 //Коэфициент приближенного расстояния
	SpeedSec                 float64 //Скорость в метрах, по городу (25 скорость км/ч, 1000 - метров)
	BuildTimeOrder           int64   //Время сборки заказа
	GiveAnOrder              int64   //Время выделяемое на отдать заказ в минутах
	ReactionCourierTimeOrder int64   //Время реакции курьера после сборки заказа
}

//Get - получение конфигурации по городу
func Get(City string) Data {
	return Data{Coefficient: 0.4, SpeedSec: 25 * 1000, BuildTimeOrder: 5, GiveAnOrder: 5, ReactionCourierTimeOrder: 4}
}
