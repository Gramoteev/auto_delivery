package redis

//Вот этот весь костыль временный, потом переключить на redis
import (
	"auto_delivery/config"
	"encoding/json"
	module_connect "ff_system/connect"
	"ff_system/connect/structExchange"
	"ff_system/function"
	"ff_system/logger"
	"ff_system/request_info"

	"sync"
	"time"
)

type Redisstr struct {
	Points sync.Map //Кеш точек
	// Couriers sync.Map //Кеш Курьеров
	// Trackers sync.Map //Кеш Треккеров

	Initialization chan bool
}

/*Точки*/
type Point struct {
	Hash          string
	Active        bool
	Lat           string //55
	Lon           string //66
	RadiusOrOrder int64
	MaxOrders     int64 //максимальное кол-во заказов
	MaxTimeRide   int64 //Максимально возможное время поездки в минутах
}

var Redis Redisstr

//
func InitLoading() {

	Redis.Initialization = make(chan bool)

	go func() {
		defer function.GetDeferPanic()
		for {
			logger.Application.Println("Алгоритм сбора первоначальных данных запущен")
			logger.Console.Println("Алгоритм сбора первоначальных данных запущен")
			err := Redis.loading()
			if err != nil {
				logger.Console.Println("Алгоритм сбора первоначальных данных выполнен с ошибкой:", err.Error())
				logger.Application.Println("Алгоритм сбора первоначальных данных выполнен с ошибкой:", err.Error())
				time.Sleep(time.Minute)
			} else {
				logger.Console.Println("Алгоритм сбора первоначальных данных успешно выполнен.")
				logger.Application.Println("Алгоритм сбора первоначальных данных выполнен.")
				break
			}
		}
	}()
}

func (self *Redisstr) loading() error {

	logger.Application.Println("Алгоритм получения и кеширования точек запущен")
	logger.Console.Println("Алгоритм получения и кеширования точек запущен")
	if err := self.loadingPoints(); err != nil {
		logger.Errors.Println("Ошибка получения точек, очередь курьеров не работает:", err)
		logger.Console.Println("Ошибка получения точек, очередь курьеров не работает:", err)
		logger.Application.Println("Ошибка получения точек, очередь курьеров не работает:", err)
		return err
	}
	logger.Console.Println("Алгоритм получения и кеширования точек успешно выполнен.")
	logger.Application.Println("Алгоритм получения и кеширования точек успешно выполнен.")

	// logger.Application.Println("Алгоритм получения и кеширования групп ролей запущен")
	// logger.Console.Println("Алгоритм получения и кеширования групп ролей запущен")
	// if err := self.loadingGroup(); err != nil {
	// 	logger.Errors.Println("Ошибка получения групп ролей, привязка куроеров и автозавершение сессий работает не корректно:", err)
	// 	logger.Console.Println("Ошибка получения групп ролей, привязка куроеров и автозавершение сессий работает не корректно:", err)
	// 	logger.Application.Println("Ошибка получения групп ролей, привязка куроеров и автозавершение сессий работает не корректно:", err)
	// 	return err
	// }
	// logger.Application.Println("Алгоритм получения и кеширования групп ролей успешно выполнен")
	// logger.Console.Println("Алгоритм получения и кеширования групп ролей успешно выполнен")

	self.Initialization <- true
	return nil
}

//LinkRedis - вернет объект с точками
func LinkRedis() *Redisstr {
	return &Redis
}

//PointStorage - получить информацию по точке
func (self *Redisstr) PointStorage(PointHash string) (p Point, ok bool) {
	var _interf interface{}
	_interf, ok = self.Points.Load(PointHash)
	if !ok {
		return
	}

	p, ok = _interf.(Point)
	if !ok {
		return
	}

	return
}

//loadingPoints - выгружает и сохраняет необходимую информацию по точкам
func (self *Redisstr) loadingPoints() error {
	var _points []Point

	message, err := module_connect.RequestNewRawValues(
		config.Config.TLS_CONNECT.OrgTls, structExchange.WMessage{
			Query:       "Select",
			RequestInfo: request_info.RequestInfo{},
			Tables: []structExchange.WTable{
				{Name: "Point", TypeParameter: "All"},
			}})
	if err != nil {
		return err
	}

	if err = json.Unmarshal(message.Tables[0].Values, &_points); err != nil {
		return err
	}

	for _, val := range _points {
		_hash := val.Hash
		val.Hash = ""
		val.RadiusOrOrder = 1000 // 1500 метров радиус некст заказа
		val.MaxOrders = 2
		val.MaxTimeRide = 60

		if val.Active {
			self.Points.Store(_hash, val)
			logger.Messaging.Println("Добавил точку [Hash: "+_hash+" ]", val)
		} else {
			logger.Messaging.Println("Не активная точка [Hash: "+_hash+" ]", val)
		}
	}
	return nil
}

// //loadingGroup - функция выгружает все необходимые группы сервису, 2 - курьеры, 7 - Треккер пиццы
// func (self *Redisstr) loadingGroup() error {

// 	var _groups []structExchange.WTable
// 	_groups = append(_groups, structExchange.WTable{Name: "Role_rgroup_front", TypeParameter: "Id_role_group_front", Values: []interface{}{2}})
// 	_groups = append(_groups, structExchange.WTable{Name: "Role_rgroup_front", TypeParameter: "Id_role_group_front", Values: []interface{}{7}})

// 	message, err := module_connect.RequestNewRawValues(
// 		config.Config.TLS_CONNECT.UserRole, structExchange.WMessage{
// 			Query:       "Select",
// 			RequestInfo: request_info.RequestInfo{}, //request_info.GetInfo(c)
// 			Tables:      _groups})
// 	if err != nil {
// 		return err
// 	}

// 	type Role_rgroup_front struct {
// 		Role_hashcode       string
// 		Id_role_group_front int
// 	}

// 	if len(message.Tables) == 0 {
// 		return errors.New("Нет добавленных ролей в группе")
// 	}

// 	for key, _ := range message.Tables {
// 		if message.Tables[key].Values != nil {
// 			var temp []Role_rgroup_front
// 			if err = json.Unmarshal(message.Tables[key].Values, &temp); err != nil {
// 				return err
// 			}
// 			for _, val := range temp {
// 				logger.Application.Println("Запись роли:", fmt.Sprintf("%+v", val))
// 				logger.Console.Println("Запись роли:", fmt.Sprintf("%+v", val))
// 				switch val.Id_role_group_front {
// 				case 2:
// 					self.Couriers.Store(val.Role_hashcode, true) //Куреьры
// 				case 7:
// 					self.Trackers.Store(val.Role_hashcode, true) //Холодный цех
// 				default:
// 					logger.Errors.Println("Пришла лишняя группа:", fmt.Sprintf("%+v", temp))
// 				}
// 			}
// 		}
// 	}
// 	return nil
// }

// func (self *Redisstr) CourierStorage(RoleHash string) bool {
// 	_, ok := self.Couriers.Load(RoleHash)
// 	return ok
// }

// func (self *Redisstr) TrackersStorage(RoleHash string) bool {
// 	_, ok := self.Trackers.Load(RoleHash)
// 	return ok
// }
