package controller

import (
	"auto_delivery/action"
	"auto_delivery/geocoder"
	"auto_delivery/postgresql"
	"auto_delivery/redis"
	"auto_delivery/redis/redisConfig"
	"database/sql"
	"errors"
	"ff_system/logger"
	"fmt"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

var OrderMaps sync.Map

//appointmentOrder - контролер работы с заказов, здесь формируется запрос на сохранение и обработку заказа, определение его в какую либо группу и сигнал назначения курьера из очереди

//Функция осуществляет поиск смежных точке, для просчета маршрута
func SearchOrder(_inTx *sqlx.Tx, _inORderStruct *action.Orders, _inGeos *geocoder.Geocoder) error {

	//Проверим есть ли уже данные по данному заказу:
	ok, err := action.NewOpportunitiesOrders().Check(_inTx, "OrderID", _inORderStruct.OrderID)
	if err != nil {
		return err
	}

	if ok {
		logger.Messaging.Println("Проверили пул подбора action.NewOpportunitiesOrders().Check() - данные уже есть")
		return nil
	}

	//Получим список заказов которые подходят для данного заказа
	ordersGroup, err := _inORderStruct.SelectRows(_inTx, "GroupsID", _inORderStruct.PointHash, pq.Int64Array(_inGeos.SearchSupposedIdGroups(_inGeos.SideOfTheWorld)), _inORderStruct.OrderID)
	if err != nil {
		return err
	}
	logger.Messaging.Println(fmt.Sprintf("Получили смежные заказы:%+v", ordersGroup))

	point, ok := redis.Redis.PointStorage(_inORderStruct.PointHash)
	if !ok {
		return fmt.Errorf("Произошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, _inORderStruct.PointHash)
	}

	//Сформируем списки приоритета возможности группировки
	//TravelingSalesman := make(map[int64]opportunitiesOrders)

	//Сформируем списки приоритета возможности группировки
	//TravelingSalesman := make(map[int64]opportunitiesOrders)

	opOrders := action.NewOpportunitiesOrders()

	//fmt.Printf("\n%+v\n", opOrders)
	IDParentOpportunities, err := opOrders.SetDB(_inTx, _inORderStruct.OrderID)
	if err != nil {
		return err
	}

	for _, val := range ordersGroup {

		logger.Messaging.Println(fmt.Sprintf("val:%+v", val))
		//
		pointStart := redis.Point{}
		pointStart.Lat = val.Lat
		pointStart.Lon = val.Lon

		//fmt.Printf("\npointStart:%+v\n", pointStart)
		geoOrTwoMark := geocoder.NewGeocoder()
		//Получим расстояния от данной точки до не приоритетной
		err = geoOrTwoMark.GetData(*_inORderStruct, pointStart)
		if err != nil {
			return err
		}
		//fmt.Printf("\ngeoOrTwoMark:%+v\n", geoOrTwoMark)

		ogol := action.OrderGroupOpportunitiesList{
			OrderID:        val.OrderID,
			Lat:            val.Lat,
			Lon:            val.Lon,
			Disstance:      geoOrTwoMark.Disstance,
			TravelTime:     geoOrTwoMark.TravelTime,
			SideOfTheWorld: geoOrTwoMark.SideOfTheWorld,
		}

		logger.Messaging.Println("val.SideOfTheWorld:", val.SideOfTheWorld, "_inORderStruct.SideOfTheWorld", _inORderStruct.SideOfTheWorld)

		var Priority int64 = 0
		//Если заказы в одной стороне
		if val.SideOfTheWorld == _inORderStruct.SideOfTheWorld {
			Priority = 1
		} else {
			//Если заказ в доступном радиусе то норм
			if geoOrTwoMark.Disstance <= point.RadiusOrOrder {
				Priority = 2
			} else {
				Priority = 3
			}
		}

		err = ogol.SetDB(_inTx, IDParentOpportunities, _inORderStruct.OrderID, Priority)
		if err != nil {
			return err
		}
	}
	return nil
}

//AddOrder - Прилетел новый заказ
func AddOrder(_inORderStruct action.Orders) error {

	tx, err := postgresql.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	err = _inORderStruct.Add(tx)
	if err != nil { //::TODO
		// err = _inORderStruct.Select(tx, "OrderID", _inORderStruct.OrderID)
		// if err != nil {
		// 	return err
		// }
		return err
	}

	//Получим координаты ресторана

	point, ok := redis.Redis.PointStorage(_inORderStruct.PointHash)
	if !ok {
		return fmt.Errorf("Произошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, _inORderStruct.PointHash)
	}

	geos := geocoder.NewGeocoder()

	err = geos.GetData(_inORderStruct, point)
	if err != nil {
		return err
	}
	_inORderStruct.SideOfTheWorld = geos.SideOfTheWorld

	//::TODO
	err = geos.SetDB(tx)
	if err != nil {
		return err
	}

	err = SearchOrder(tx, &_inORderStruct, geos)
	if err != nil {
		return err
	}

	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

//--checkDeliveryTime - проверяем успевает ли курьер доставить заказ, (_inTravelTime - время на доставку, _inStartTime - время начала отсчета, _inEndTime - предельное время)
func checkDeliveryTime(_inTravelTime int64, _inStartTime time.Time, _inEndTime time.Time) bool {
	//Проверим временные зоны
	logger.Messaging.Println("Сейчас", _inStartTime)
	endDeliveryTime := _inStartTime.Add(time.Duration(_inTravelTime) * time.Minute)
	logger.Messaging.Println("После доставки заказ", endDeliveryTime)
	if _inEndTime.Before(endDeliveryTime) {
		logger.Messaging.Println("Курьер успеет доставить только 1 заказ", endDeliveryTime)
		return false
	}

	return true
}

func getDifference(_inStatusID int64, _inTimeCooked, _inRealTime time.Time) (differenceTime int64) {
	if _inStatusID < 8 {
		//Получим разницу в минутах
		//differenceTime = int64(realTime.Sub(ogol.TimeCooked).Minutes())
		differenceTime = int64(_inTimeCooked.Sub(_inRealTime).Minutes())
		logger.Messaging.Println("Получили разницу: ", differenceTime)
		if differenceTime < 0 {
			differenceTime = 0
			logger.Messaging.Println("Обнуляем разницу: ", differenceTime)
		} else if differenceTime > 8 {
			differenceTime = 999
			logger.Messaging.Println("Разница больше 8 минут")
		}
	}
	return
}

//_inORderStruct action.Orders
func UpStatusOrder(_inORderStruct *action.Orders) error {
	logger.Messaging.Println("----------------- UpStatusOrder -----------------")

	tx, err := postgresql.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	ORderStructOld := action.NewOrders()
	if err := ORderStructOld.Select(tx, "OrderID", _inORderStruct.OrderID); err != nil {
		return err
	}

	logger.Messaging.Println("Новый статус _inORderStruct.StatusID: ", _inORderStruct.StatusID)
	logger.Messaging.Println("Получили дополнительную информацию по базовому заказу:", fmt.Sprintf("%+v", ORderStructOld))

	if _inORderStruct.StatusID == ORderStructOld.StatusID && (_inORderStruct.StatusID != 11 && ORderStructOld.StatusID != 11) && _inORderStruct.StatusID != 15 && _inORderStruct.StatusID != 16 {
		return errors.New("Статус совпадает с предыдущим")
	} /*else  if ORderStructOld.StatusID > _inORderStruct.StatusID && _inORderStruct.StatusID != 21 && _inORderStruct.StatusID != 15 && _inORderStruct.StatusID != 16 {
		return errors.New("Предыдущий статус больше нового")
	}*/

	if _inORderStruct.StatusID == 21 { //Если это переделка то надо действовать так

		//Проверяем получили ли время готовки
		if _inORderStruct.TimeCooked.IsZero() {
			return errors.New("При постановке статуса переделка, время на переделку отправлено пустое")
		}

		//Проверяем лаг больше 4 минут или нет?
		// if math.Abs((_inORderStruct.TimeCooked.Sub(ORderStructOld.TimeCooked).Minutes())) < 4 {
		// 	logger.Messaging.Println("Разница во времени меньше 4 минут, подождем и ничего менять не будем")
		// 	return nil
		// }

		//Если больше, то продолжаем откат:

		err = ManagerIntervenes21(tx, _inORderStruct)
		if err != nil {
			return err
		}

	} else {
		ORderStructOld.StatusID = _inORderStruct.StatusID
		_inORderStruct = ORderStructOld

		err = _inORderStruct.UpStatus(tx)
		if err != nil {
			return err
		}
		logger.Messaging.Println("Выполнили обновление UpStatus")

		//Если статус 9, т.е. заказ собран назначаем курьера.
		logger.Messaging.Println("Статус для обновления:", _inORderStruct.StatusID)
	}

	if _inORderStruct.StatusID == 9 {

		//Проверим находится ли этот заказ уже в пачке

		oad := action.NewOrdersAwaitingDelivery()

		check, err := oad.Check(nil, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}
		//Заказ найден в очереди
		if check {

			logger.Messaging.Println("Заказ уже есть в очереди ожидания, передаем в назначение курьера")
			//Назначаем курьера
			err = AssignmentOfOrdersToTheCourier(tx, _inORderStruct.PointHash, Params{EventCourier: true, CollectionOfBundlesAwaitingDelivery: false})
			if err != nil {
				return err
			}

			//Тут комитим в случае, если нет свободных курьеров или нет заказов и курьер просто попадает в очередь
			err = tx.Commit()
			if err != nil {
				return err
			}
			logger.Messaging.Println("Ошибок нет, закомитили")

			return nil
		}

		point, ok := redis.Redis.PointStorage(_inORderStruct.PointHash)
		if !ok {
			return fmt.Errorf("\nПроизошла ошибка при получении информации о точке [redis.Redisstr.PointStorage]: %t, точка %s", ok, _inORderStruct.PointHash)
		}

		mtkEnd := false
		logger.Messaging.Println("Получили информацию по точке:", fmt.Sprintf("%+v", point))

		pulOrders := []int64{_inORderStruct.OrderID}

		AllOrders := make(map[int64]action.OrderGroupOpportunitiesList)

		if point.MaxOrders <= 1 {
			logger.Messaging.Println("Необходим один или менее заказ")
			//::TODO - назначаем один заказ и едем, если же не один то идем дальше
			mtkEnd = true
		} else {

			logger.Messaging.Println("Начинаем искать заказ для добавления")

			//Ищем подходящий заказ
			opOrders := action.NewOpportunitiesOrders()
			ordersGroup, err := opOrders.GetDB("OrderID", _inORderStruct.OrderID)
			if err != nil {
				return err
			}
			logger.Messaging.Println("Получил заказы которые можно будет добавить ordersGroup:", fmt.Sprintf("%+v", ordersGroup))

			geosBaseOrder := geocoder.NewGeocoder()
			if err := geosBaseOrder.Select(_inORderStruct.OrderID); err != nil {
				return err
			}

			paramsCity := redisConfig.Get("")
			paramsCity.BuildTimeOrder = paramsCity.BuildTimeOrder / 2

			realTime := time.Now().UTC()

			//Проверим доставки основного заказа = время реакции курьера после назначения + время доставки самого заказа
			ok := checkDeliveryTime(paramsCity.ReactionCourierTimeOrder+geosBaseOrder.TravelTime, realTime, _inORderStruct.TimeDelivery)
			if ok { //Если на первый заказ успеваем, то продолжаем поиск
				logger.Messaging.Println("Получил информацию по местоположению базового заказа: ", fmt.Sprintf("%+v", geosBaseOrder))

				var PriorityArr []action.OrderGroupOpportunitiesList
				var RadiusArr []action.OrderGroupOpportunitiesList
				var LowPriorityArr []action.OrderGroupOpportunitiesList

				for _, val := range ordersGroup {
					AllOrders[val.OrderID] = val
					switch val.Priority {
					case 1:
						//opOrders.Priority[val.OrderID] = val
						//PriorityArr = append(PriorityArr, val)
						RadiusArr = append(RadiusArr, val)
					case 2:
						//opOrders.Radius[val.OrderID] = val
						RadiusArr = append(RadiusArr, val)
					case 3:
						//opOrders.LowPriority[val.OrderID] = val
						LowPriorityArr = append(LowPriorityArr, val)
					}
				}

				logger.Messaging.Println("\nДо\nСформировал пул PriorityArr: ", fmt.Sprintf("%+v", PriorityArr))
				logger.Messaging.Println("Сформировал пул RadiusArr: ", fmt.Sprintf("%+v", RadiusArr))
				logger.Messaging.Println("Сформировал пул LowPriorityArr: ", fmt.Sprintf("%+v", LowPriorityArr))
				opOrders.Sort(PriorityArr)
				opOrders.Sort(RadiusArr)
				opOrders.Sort(LowPriorityArr)
				logger.Messaging.Println("\nПосле\nСформировал пул PriorityArr: ", fmt.Sprintf("%+v", PriorityArr))
				logger.Messaging.Println("Сформировал пул RadiusArr: ", fmt.Sprintf("%+v", RadiusArr))
				logger.Messaging.Println("Сформировал пул LowPriorityArr: ", fmt.Sprintf("%+v", LowPriorityArr))

				var sumTravelTime int64

				logger.Messaging.Println("\nНачинаем перебор RadiusArr")

				//Берем какой нибудь заказ в радиусе
				for _, order := range RadiusArr {

					if mtkEnd {
						break
					}

					if int64(len(pulOrders)) >= point.MaxOrders {
						logger.Messaging.Println(fmt.Sprintf("Достигнуто максимальное кол-во заказов: %+v", point.MaxOrders))
						mtkEnd = true
						break
					}

					logger.Messaging.Println(fmt.Sprintf("Взял заказ %+v", order))

					logger.Messaging.Println("Складываем: ", geosBaseOrder.TravelTime, " ", order.Disstance)
					ttl := getDifference(order.StatusID, order.TimeCooked, realTime) + paramsCity.BuildTimeOrder + sumTravelTime + order.TravelTime + paramsCity.GiveAnOrder

					//Кол-во добавочных минут (время готовки + время сборки  + время отдачи клиенту)
					addTimeAdditional := getDifference(order.StatusID, order.TimeCooked, realTime) + paramsCity.BuildTimeOrder + paramsCity.GiveAnOrder

					if ttl > point.MaxTimeRide {
						logger.Messaging.Println("Максимальное время доставки [", point.MaxTimeRide, "] превышено")
						continue
					}

					//Проверим второй заказ
					if ok := checkDeliveryTime(order.TravelTime+addTimeAdditional, realTime.Add(time.Duration(sumTravelTime)*time.Minute), order.TimeDelivery); !ok {
						logger.Messaging.Println("Не успевает на второй заказ")
						continue
					}

					// sumTravelTime = ttl

					pulOrders = append(pulOrders, order.OrderID)

					sumTravelTime = ttl

				}

				//Если из приоритетов ничего не добавлялось, сохраняем время первой поездки
				if sumTravelTime == 0 {
					sumTravelTime += paramsCity.ReactionCourierTimeOrder + geosBaseOrder.TravelTime
				}

				logger.Messaging.Println("\nНачинаем перебор PriorityArr")
				logger.Messaging.Println("sumTravelTime Radius:", sumTravelTime)

				for _, order := range PriorityArr {

					if mtkEnd {
						break
					}

					if int64(len(pulOrders)) >= point.MaxOrders {
						logger.Messaging.Println(fmt.Sprintf("Достигнуто максимальное кол-во заказов: %+v", point.MaxOrders))

						//Проверим, может есть заказ совсем рядом
						// if int64(len(pulOrders)) >= point.MaxOrders+1 {
						// 	mtkEnd = true
						// 	break
						// } else {

						// 	continue
						// }

						mtkEnd = true
						break
					}

					logger.Messaging.Println(fmt.Sprintf("Взял заказ %+v", order))

					//Если от предполагаемого добавленного заказа растояние от точки меньше, то и время пути необходимо пересчитать
					logger.Messaging.Println("Базовая дальность второго заказа от точки [order.TravelTimeBasePoint]: ", order.TravelTimeBasePoint, ", Основого заказа [geosBaseOrder.TravelTime]: ", geosBaseOrder.TravelTime)
					logger.Messaging.Println("order.TravelTimeBasePoint < geosBaseOrder.TravelTime: ", order.TravelTimeBasePoint < geosBaseOrder.TravelTime)

					logger.Messaging.Println("order.StatusID: ", order.StatusID)

					//Получил сколько времени надо до конца готовки
					var differenceTime int64 = getDifference(order.StatusID, order.TimeCooked, realTime)

					//Кол-во добавочных минут (время готовки + время сборки + время отдачи клиенту)
					addTimeAdditional := differenceTime + paramsCity.BuildTimeOrder + paramsCity.GiveAnOrder
					if order.TravelTimeBasePoint < geosBaseOrder.TravelTime {

						logger.Messaging.Println("order.StatusID: ", order.StatusID)

						//TravelTimeSwap := geosBaseOrder.TravelTime - order.TravelTimeBasePoint //Получили дистанцию до первого заказа
						logger.Messaging.Println("Складываем: ", order.TravelTimeBasePoint, " ", order.TravelTime)
						//Время на сборку курьера + время с точки до первой точки назначения + До  второй (которая была первой) + время отдачи
						ttl := addTimeAdditional + order.TravelTimeBasePoint + order.TravelTime

						if ttl > point.MaxTimeRide {
							logger.Messaging.Println("Максимальное время доставки [", point.MaxTimeRide, "] превышено, поэтому заказ не подходит")
							continue
						}

						//Проверим успеем ли вообще на данные заказы
						//Проверим первый заказ

						if ok := checkDeliveryTime(paramsCity.ReactionCourierTimeOrder+order.TravelTimeBasePoint, realTime, order.TimeDelivery); !ok {
							logger.Messaging.Println("Второй заказ при свапе не успеет, будет кешбек")
							continue
						}

						//Проверим второй заказ
						if ok := checkDeliveryTime(order.TravelTime+addTimeAdditional, realTime.Add(time.Duration(paramsCity.ReactionCourierTimeOrder+order.TravelTimeBasePoint)*time.Minute), _inORderStruct.TimeDelivery); !ok {
							logger.Messaging.Println("Не успевает на второй заказ, т.е. первоначальный")
							continue
						}

						sumTravelTime = sumTravelTime + ttl
					} else {

						logger.Messaging.Println("Складываем: ", geosBaseOrder.TravelTime, " ", order.TravelTime)
						ttl := differenceTime + paramsCity.BuildTimeOrder + geosBaseOrder.TravelTime + order.TravelTime + paramsCity.GiveAnOrder

						if ttl > point.MaxTimeRide {
							logger.Messaging.Println("Максимальное время доставки [", point.MaxTimeRide, "] превышено")
							continue
						}

						//Проверим второй заказ
						if ok := checkDeliveryTime(order.TravelTime+addTimeAdditional, realTime.Add(time.Duration(geosBaseOrder.TravelTime)*time.Minute), order.TimeDelivery); !ok {
							logger.Messaging.Println("Не успевает на второй заказ")
							continue
						}

						sumTravelTime = sumTravelTime + ttl
					}
					pulOrders = append(pulOrders, order.OrderID)
				}

				logger.Messaging.Println("sumTravelTime Priority:", sumTravelTime)

				//Берем какой нибудь заказ в радиусе
				for _, order := range LowPriorityArr {

					if mtkEnd {
						break
					}

					if int64(len(pulOrders)) >= point.MaxOrders {
						logger.Messaging.Println(fmt.Sprintf("Достигнуто максимальное кол-во заказов: %+v", point.MaxOrders))
						mtkEnd = true
						break
					}

					logger.Messaging.Println(fmt.Sprintf("Взял заказ %+v", order))

					logger.Messaging.Println("Дистанция: ", order.Disstance)
					//										Складываем:   		0						 2 				8 				11 					5
					logger.Messaging.Println("Складываем: ", getDifference(order.StatusID, order.TimeCooked, realTime), paramsCity.BuildTimeOrder, sumTravelTime, order.TravelTime, paramsCity.GiveAnOrder)
					ttl := getDifference(order.StatusID, order.TimeCooked, realTime) + paramsCity.BuildTimeOrder + sumTravelTime + order.TravelTime + paramsCity.GiveAnOrder

					//Кол-во добавочных минут (время готовки + время сборки  + время отдачи клиенту)
					addTimeAdditional := getDifference(order.StatusID, order.TimeCooked, realTime) + paramsCity.BuildTimeOrder + paramsCity.GiveAnOrder
					logger.Messaging.Println("addTimeAdditional: ", addTimeAdditional)
					if ttl > point.MaxTimeRide {
						logger.Messaging.Println("Максимальное время доставки [", point.MaxTimeRide, "] превышено")
						continue
					}

					//Проверим второй заказ
					if ok := checkDeliveryTime(order.TravelTime+addTimeAdditional, realTime.Add(time.Duration(sumTravelTime)*time.Minute), order.TimeDelivery); !ok {
						logger.Messaging.Println("Не успевает на второй заказ")
						continue
					}

					pulOrders = append(pulOrders, order.OrderID)

					sumTravelTime = ttl

				}

				logger.Messaging.Println("sumTravelTime:", sumTravelTime)
			}

		}

		//---------------------///--------------------------//
		//Формируем пачку заказов и сохраняем

		logger.Messaging.Println("SelectLastPackID")
		packID, err := oad.SelectLastPackID()
		if err != nil {
			if err == sql.ErrNoRows {
				packID = 0
			} else {
				return err
			}
		}
		packID = packID + 1

		oad.Pack = packID
		oad.PointHash = _inORderStruct.PointHash
		oad.TimeInsert = time.Now()

		logger.Messaging.Println("oad.SetDB()")
		if err := oad.SetDB(tx); err != nil {
			return err
		}

		oadl := action.NewOrdersAwaitingDeliveryList()
		oadl.PackID = packID
		for _, OrderID := range pulOrders {

			if OrderID == _inORderStruct.OrderID {
				oadl.OrderID = _inORderStruct.OrderID
			} else {
				_order, ok := AllOrders[OrderID]
				if !ok {
					return errors.New("Не найден ID заказа в пуле заказов")
				}
				oadl.OrderID = _order.OrderID
			}

			err := oadl.SetDB(tx)
			if err != nil {
				return err
			}

			noo := action.NewOpportunitiesOrders()

			logger.Messaging.Println("noo.Delete():", oadl.OrderID)
			err = noo.Delete(tx, "OrderID", oadl.OrderID)
			if err != nil {
				return err
			}
		}

		//Назначаем курьера
		err = AssignmentOfOrdersToTheCourier(tx, _inORderStruct.PointHash, Params{EventCourier: true, CollectionOfBundlesAwaitingDelivery: false})
		if err != nil {
			return err
		}

	} //Условие статуса

	if _inORderStruct.StatusID == 11 || _inORderStruct.StatusID == 15 || _inORderStruct.StatusID == 16 {
		err = ManagerIntervenes(tx, _inORderStruct)
		if err != nil {
			return err
		}
	}

	if _inORderStruct.StatusID == 10 {
		courier := action.NewCouriers()
		infoCourier, err := courier.SelectAll(tx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}
		if len(infoCourier) > 0 {
			err = eventCourier(tx, "UserHash", infoCourier[0].UserHash)
			if err != nil {
				return err
			}
		}
	}

	logger.Messaging.Println("Закрываем транзакцию")
	err = tx.Commit()
	if err != nil {
		return err
	}

	return nil
}

//ManagerIntervenes - менеджер вмешивается
func ManagerIntervenes(_inTx *sqlx.Tx, _inORderStruct *action.Orders) error {
	//Переназначает заказ или назначает на заказ курьера сама

	//Если заказ уже назначен на кого то, ставим ему 11 статус.
	var err error

	if _inTx == nil {
		return errors.New("Функция ManagerIntervenes работает только при открытой транзакции")
	}

	//Заказ ожидает доставки? - Пачка сформирована но еще не отдана
	ok, err := action.NewOrdersAwaitingDelivery().Check(_inTx, "OrderID", _inORderStruct.OrderID)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	logger.Messaging.Println("action.NewOrdersAwaitingDelivery().Check:", ok)
	if ok { //Если ожидает назначения, то обнуляем его и ещем ему новый вариант

		logger.Messaging.Println("Заказ ожидает назначения доставки, узнаем один ли он в своей пачке")
		arrOADL, err := action.NewOrdersAwaitingDeliveryList().Query(_inTx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}

		logger.Messaging.Println("Получаю кол-во в паке [action.NewOrdersAwaitingDeliveryList().Query]:", ok)
		if len(arrOADL) == 1 { //Заказ в пачке один, смело чистим
			logger.Messaging.Println("Заказ один")
			err = action.NewOrdersAwaitingDelivery().Delete(_inTx, "PackID", arrOADL[0].PackID)
			if err != nil {
				return err
			}
		} else { // Заказ не один, просто убираем его и вызываем подбор друга
			logger.Messaging.Println("Заказов несколько")
			err = action.NewOrdersAwaitingDeliveryList().Delete(_inTx, "OrderID", _inORderStruct.OrderID)
			if err != nil {
				return err
			}

			if _inORderStruct.StatusID <= 9 {
				logger.Messaging.Println("Так как статус <= 9, то ищем друга")
				//Тут подбор друга должен начаться
				//:TODO
			}
		}
	} else {

		//Проверяем может уже доставляется?
		nc := action.NewCouriers()
		ok, err = nc.Check(_inTx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}
		courier := action.NewCouriers()
		infoCourier, err := courier.SelectAll(_inTx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}

		if ok { //Уже назначен курьер на пак заказа

			logger.Messaging.Println("Заказ уже назначен на курьера")
			cur := action.NewCouriers() //Просто закрываем его
			if err := cur.SetOrderDelivered(_inTx, _inORderStruct.OrderID); err != nil {
				return err
			}
			logger.Messaging.Println("Доставили заказ _inORderStruct.OrderID отправляем в брокер")

			if len(infoCourier) > 0 {
				err = eventCourier(_inTx, "UserHash", infoCourier[0].UserHash)
				if err != nil {
					return err
				}
			}

		} else { //Заказ еще нигде не болтается, просто забываем о нём

			logger.Messaging.Println("Заказ не назначен и не сформированна пачка, забываем")
			noo := action.NewOpportunitiesOrders()
			logger.Messaging.Println("noo.Delete():", _inORderStruct.OrderID)
			err = noo.Delete(_inTx, "OrderID", _inORderStruct.OrderID)
			if err != nil {
				return err
			}

		}

	}

	//Если заказ ждем назначения, то просто убираем его

	//Далее менеджер отдала заказ какому то курьру, его надо убрать из очереди, если он там находится.
	return nil
}

//ManagerIntervenes21 - Заказ отправлен на переделку
func ManagerIntervenes21(_inTx *sqlx.Tx, _inORderStruct *action.Orders) error {

	logger.Messaging.Println("ManagerIntervenes21")
	var err error

	if _inTx == nil {
		return errors.New("Функция ManagerIntervenes21 работает только при открытой транзакции")
	}

	//Заказ ожидает доставки? - Пачка сформирована но еще не отдана
	ok, err := action.NewOrdersAwaitingDelivery().Check(_inTx, "OrderID", _inORderStruct.OrderID)
	if err != nil && err != sql.ErrNoRows {
		return err
	}

	logger.Messaging.Println("action.NewOrdersAwaitingDelivery().Check:", ok)
	if ok { //Если ожидает назначения, то обнуляем его и откатываем

		logger.Messaging.Println("Заказ ожидает назначения доставки, узнаем один ли он в своей пачке")
		arrOADL, err := action.NewOrdersAwaitingDeliveryList().Query(_inTx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}

		logger.Messaging.Println("Получаю кол-во в паке [action.NewOrdersAwaitingDeliveryList().Query]:", ok)
		if len(arrOADL) == 1 { //Заказ в пачке один, смело чистим
			logger.Messaging.Println("Заказ один")
			err = action.NewOrdersAwaitingDelivery().Delete(_inTx, "PackID", arrOADL[0].PackID)
			if err != nil {
				return err
			}
		} else { // Заказ не один, просто убираем его и вызываем подбор друга
			logger.Messaging.Println("Заказов несколько")
			err = action.NewOrdersAwaitingDeliveryList().Delete(_inTx, "OrderID", _inORderStruct.OrderID)
			if err != nil {
				return err
			}

			// if _inORderStruct.StatusID <= 9 {
			// 	logger.Messaging.Println("Так как статус <= 9, то ищем друга")
			// 	//Тут подбор друга должен начаться
			// 	//:TODO
			// }
		}
	} else {

		//Проверяем может уже доставляется?
		nc := action.NewCouriers()
		ok, err = nc.Check(_inTx, "OrderID", _inORderStruct.OrderID)
		if err != nil {
			return err
		}
		if ok { //Уже назначен курьер на пак заказа

			logger.Messaging.Println("Заказ уже назначен на курьера, значит нужно произвести откат")
			cpo := action.CouriersPackOrders{}
			Orders, err := cpo.Query(_inTx, "OrderIDorPackID", _inORderStruct.OrderID)
			if err != nil {
				return err
			}
			logger.Messaging.Println(fmt.Sprintf("Получили заказы %+v", Orders))
			if len(Orders) == 1 {
				logger.Messaging.Println("Заказ был один в пачке, удаляем пачку")
				err = cpo.Delete(_inTx, "PackID", Orders[0].ID)
				if err != nil {
					return err
				}
				logger.Messaging.Println("Далее, обнуляем курьера", Orders[0].UserHash)
				//Обнулим курьера
				err = nc.UpdatePrioritysNoValidation(_inTx, Orders[0].UserHash)
				if err != nil {
					return err
				}
				logger.Messaging.Println("Обнулили")

			} else {

				logger.Messaging.Println("cpo.Rollback - удаляем заказ из истории пачек и закрываем саму пачку, если заказов более не осталось")
				err = cpo.Rollback(_inTx, "OrderID", _inORderStruct.OrderID)
				if err != nil {
					return err
				}

			}

			err = eventCourier(_inTx, "UserHash", Orders[0].UserHash)
			if err != nil {
				return err
			}

		}

	}

	/***********************************/
	logger.Messaging.Println("Заказ не назначен и не сформированна пачка, значит просто меняем время и ставим 4-ый статус")
	no := action.NewOrders()
	logger.Messaging.Println("noo.Delete():", _inORderStruct.OrderID)
	no.OrderID = _inORderStruct.OrderID
	no.StatusID = 4
	no.TimeCooked = _inORderStruct.TimeCooked
	err = no.UpOrder(_inTx)
	if err != nil {
		return err
	}
	/***********************************/

	geos := geocoder.NewGeocoder()
	err = geos.Select(_inORderStruct.OrderID)
	if err != nil {
		return err
	}

	//Ставим ему новые взимосвязи
	err = SearchOrder(_inTx, _inORderStruct, geos)
	if err != nil {
		return err
	}

	//Если заказ ждем назначения, то просто убираем его

	//Далее менеджер отдала заказ какому то курьру, его надо убрать из очереди, если он там находится.
	return nil
}
