package controller

import (
	"auto_delivery/action"
	"auto_delivery/geocoder"
	"auto_delivery/postgresql"
	"auto_delivery/redis"
	"auto_delivery/redis/redisConfig"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"ff_system/logger"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
)

//appointmentCouriers.go - контролер работы с очередью и выдачей заказов

//AssignmentOfOrdersToTheCourier - функция берет заказ из сформированного пула и назначает его на свободного курьера курьера
func AssignmentOfOrdersToTheCourier(_inTx *sqlx.Tx, _inPointHash string, _inParams Params) error {

	if _inTx == nil {
		return errors.New("Функция AssignmentOfOrdersToTheCourier поддерживает работу только с открытой транзакцией")
	}

	logger.Messaging.Println(fmt.Sprintf("Параметры _inParams: %+v", _inParams))

	logger.Messaging.Println("Начинаем поиск курьера")
	//func (с *Courier) Select(_inType string, _InArgs ...interface{}) (err error) {
	courier := action.NewCouriers()
	err := courier.Select("Queue", _inPointHash)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Messaging.Println("Нет свободного курьера")
			return nil
		} else {
			return err
		}
	}

	if len(courier.UserName) == 0 {
		logger.Messaging.Println("courier.UserName - пустой, ошибка получения данных по курьеру при назначении заказа")
		return nil
	}
	logger.Messaging.Println("Взял курьера из очереди:", courier.UserName)

	// if len(_inCourier.PointHash) == 0 {
	// 	return fmt.Errorf("У данного курьера не указана точка: %s", _inCourier.PointHash)
	// }

	// _inTx.Commit()
	// panic("1")
	//Берем первый заказ из списка
	noad := action.NewOrdersAwaitingDelivery()
	OrdersAll, err := noad.Query(_inTx, "FirstPack", _inPointHash)
	if err != nil {
		if err == sql.ErrNoRows {
			logger.Messaging.Println("Нет заказов для назначения Select")
			return nil
		} else {
			return err
		}
	}

	if len(OrdersAll) == 0 {
		logger.Messaging.Println("Нет заказов для назначения len(Orders)")
		return nil
	}

	var Orders []action.OrdersAwaitingDelivery
	var firstPack int64

	//Собираем первую пачку, пока не выберем всех из первого пака
	for _, order := range OrdersAll {
		if firstPack == 0 {
			firstPack = order.Pack
		} else {
			if firstPack != order.Pack {
				break
			}
		}
		Orders = append(Orders, order)
	}

	logger.Messaging.Println("len(Orders) == ", len(Orders))

	//Проверим нужна ли дополнительная группировка по сформированным пачка, работает если заказ в назначаемой пачке 1
	if _inParams.CollectionOfBundlesAwaitingDelivery && len(Orders) == 1 {

		__baseOrder := Orders[0]

		logger.Messaging.Println("Назначение курьера идет с опозданием, проверяем ненужно ли сгруппировать какие либо пачки с пачкой этого заказа:", fmt.Sprintf("%+v", __baseOrder))
		//Проверим, можно ли сгруппировать какие либо пачки заказов
		oadArr, err := action.NewOrdersAwaitingDelivery().Query(_inTx, "AllPackSoloOrderNotPack", _inPointHash, __baseOrder.Pack)
		if err != nil {
			return err
		}
		logger.Messaging.Println("Получили пачки:", fmt.Sprintf("%+v", oadArr))

		if len(oadArr) > 0 {
			//Получим смежные зоны
			SideOfTheWorldZones := pq.Int64Array(geocoder.NewGeocoder().SearchSupposedIdGroups(__baseOrder.SideOfTheWorld))
			logger.Messaging.Println("Получили смежные зоны: ", SideOfTheWorldZones)
			usingPack := make(map[int64]int64) //Фисируем использованные паки на всякий случай
			//Соберем паки которые содержат один заказ

			//Сформируем дополнительное время
			paramsCity := redisConfig.Get("")
			paramsCity.BuildTimeOrder = paramsCity.BuildTimeOrder / 2

			realTime := time.Now().UTC()

			for _, oadOrder := range oadArr {
				_, ok := usingPack[oadOrder.Pack]
				if ok {
					logger.Errors.Println("Данный пак уже просматривали, ошибка, но просто пропустим его")
					continue
				}

				addTimeAdditional := getDifference(oadOrder.StatusID, oadOrder.TimeCooked, realTime) + paramsCity.BuildTimeOrder + paramsCity.GiveAnOrder

				logger.Messaging.Println("Взял заказ из пула, для проверки:", fmt.Sprintf("%+v", oadOrder))

				if geocoder.NewGeocoder().CheckSupposedIdGroups(oadOrder.SideOfTheWorld, SideOfTheWorldZones) {

					//Если зона подходящая берем заказ
					logger.Messaging.Println("Зона подходящая")

					//Уточняем, время поездки
					geos := geocoder.NewGeocoder()
					err = geos.GetData(action.Orders{Lat: oadOrder.Lat, Lon: oadOrder.Lon}, redis.Point{Lat: __baseOrder.Lat, Lon: __baseOrder.Lon})
					//Проверяем успевает ли на первый заказ
					if err != nil {
						return err
					}
					logger.Messaging.Println("Получил время поездки от первого заказа, до выбранного из пула:", geos.TravelTime)
					//order.TravelTime+addTimeAdditional, realTime.Add(time.Duration(paramsCity.ReactionCourierTimeOrder+order.TravelTimeBasePoint)*time.Minute), _inORderStruct.TimeDelivery
					if ok := checkDeliveryTime(addTimeAdditional+geos.TravelTime, realTime.Add(time.Duration(__baseOrder.TravelTime)*time.Minute), oadOrder.TimeDelivery); !ok {
						logger.Messaging.Println("Не успевает на заказ из данной пачки")
						continue
					}

					Orders = append(Orders, oadOrder)
					break

				}
			}
		} else {
			logger.Messaging.Println("Пул группировок OrdersAwaitingDelivery <= 0, ничего делать не надо")
		}
	}

	arrOrder := []int64{}

	//Собираем нужные заказы
	for _, order := range Orders {
		arrOrder = append(arrOrder, order.OrderID)
	}

	logger.Messaging.Println("arrOrder:", arrOrder)

	err = courier.SetOrders(_inTx, courier.UserHash, arrOrder)
	if err != nil {
		return err
	}

	for _, order := range Orders {
		err = noad.Delete(_inTx, "PackID", order.Pack)
		if err != nil {
			return err
		}
	}

	//Отдаем заказы в ответ через брокер

	// type OrderOfCur struct {
	// 	OrderID  int64
	// 	Userhash string
	// 	PackID   int64
	// }

	// OOC := []OrderOfCur{}

	// for _, val := range Orders {
	// 	OOC = append(OOC, OrderOfCur{val.OrderID, courier.UserHash, Orders[0].Pack})
	// }

	// err = action.Broker.Send("autoDelivery", &structExchange.WMessage{
	// 	Query: "Insert",
	// 	Tables: []structExchange.WTable{
	// 		{
	// 			Name:          "opportunitiesOrders",
	// 			TypeParameter: "",
	// 			Values:        OOC,
	// 		},
	// 	},
	// })
	// if err != nil {
	// 	return err
	// }

	//return errors.New("asdasd")

	//Теперь оповещаем Сервис курьера об изменениях
	if _inParams.EventCourier {
		err = eventCourier(_inTx, "UserHash", courier.UserHash)
		if err != nil {
			return err
		}
	}

	return nil
}

//SetNewCourier - Курьер появился в системе
func SetNewCourier(_inCour action.Courier) error {
	err := _inCour.Set()
	if err != nil {
		return err
	}

	err = eventCourier(nil, "UserHash", _inCour.UserHash)
	if err != nil {
		return err
	}

	return nil
}

//SetCourierAutoDelivery - Курьеру ставится флаг
func SetCourierAutoDelivery(_inCour action.Courier) error {
	err := _inCour.Update(nil, "AutoDelivery")
	if err != nil {
		return err
	}

	err = eventCourier(nil, "UserHash", _inCour.UserHash)
	if err != nil {
		return err
	}

	return nil
}

//SetCourierQueue - Ставиим курьеру статус, что он готов
func SetCourierQueue(_inCour action.Courier) error {
	if err := _inCour.UpdatePrioritys(); err != nil {
		return err
	}

	tx, err := postgresql.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	err = AssignmentOfOrdersToTheCourier(tx, _inCour.PointHash, Params{EventCourier: false, CollectionOfBundlesAwaitingDelivery: true})
	if err != nil {
		return err
	}

	err = eventCourier(tx, "UserHash", _inCour.UserHash)
	if err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func eventCourier(tx *sqlx.Tx, _inType string, _inParam ...interface{}) error {

	courier := action.NewCouriers()

	logger.Messaging.Println("eventCourier")
	logger.Messaging.Println("eventCourier", _inParam)
	if _inType != "UserHash" {
		return errors.New("Ошибка eventCourier _inType !=  UserHash")
	}

	infoCourier, err := courier.SelectAll(tx, _inType, _inParam...)
	if err != nil {
		return err
	}

	logger.Messaging.Println("infoCourier", fmt.Sprintf("%+v", infoCourier))
	if len(infoCourier) == 0 {
		return nil
	}

	logger.Messaging.Println("Отправляю на брокер", infoCourier[0].PointHash)
	err = action.SendEventCourier(infoCourier[0].PointHash, infoCourier)
	if err != nil {
		return err
	}

	return nil

}
