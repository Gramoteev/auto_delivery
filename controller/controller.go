package controller

import (
	"auto_delivery/action"
	"auto_delivery/postgresql"
	"auto_delivery/redis"
	"ff_system/logger"
)

//тут будет функция взаимодействяи с брокером
func Broker() {
	//Тут будет запуск на синхронизацию в реальном времени
}

//initMutexCouriers - Инициализация мьютексов
func initMutexCouriers() {

	logger.Messaging.Println("Ждем подтвержения завeршения получения точек")
	<-redis.Redis.Initialization
	logger.Messaging.Println("Точки получены")

	redis.Redis.Points.Range(func(key, value interface{}) bool {
		action.CouriersMutex.Set(key.(string))
		logger.Messaging.Println("Создан мьютекс")
		return true
	})
}

func initDB() {
	postgresql.InitDatabase()
}

func initRedis() {
	redis.InitLoading()
}
