package controller

type Params struct {
	EventCourier                        bool // Определяет отправять уведомление в брокер на обновление курьера или нет
	CollectionOfBundlesAwaitingDelivery bool // Пытаться сгруппировать пачки ожидающие доставки
}
