package controller

import (
	"auto_delivery/action"
)

func InitController() {

	initDB()

	initRedis()

	initMutexCouriers()

	Broker()

	action.InitStock()
}
