package config

// Получение настроек из файла конфигураций

import (
	"ff_system/config"
	"ff_system/logger"
	"ff_system/servisconfig"
	"fmt"
	"sync"
)

//Объявление структуры конфигураций.
type Configurations struct {
	DB struct {
		DBLogin, DBPass string
		DBHost          string
		DBName          string
		DBSsl           string
	}
	TLS_SERVER struct {
		TlsHost, TlsPort string
	}
	TLS_CONNECT struct {
		OrderTls string
		UserTls  string
		OrgTls   string
	}
	LOGGING struct {
		LoggingConsole     bool
		LoggingApplication bool
		LoggingMessaging   bool
		LoggingErrors      bool
	}
	BROKER struct {
		RabbitLogin, RabbitPassword string
	}
	FFSystem config.Configuration
}

var ConfigMutex sync.RWMutex
var Config Configurations
var Location string
var FileName string

func init() {
	var err error
	Location, FileName, err = servisconfig.GetConfigurationsStruct(&Config)
	if err != nil {
		fmt.Println("Ошибка получения конфигурационного файла!:", err)
		panic(err)
	}

	if !servisconfig.ValidationConfig(Config) {
		fmt.Printf("\n ValidationConfig FALSE!! %+v, %+v, %+v", Location, FileName, Config)
		panic("ValidationConfig FALSE!:")
	}

	err = servisconfig.InitConfigFFSystem(Config)
	if err != nil {
		fmt.Println("Ошибка инициализации конфигурации ff_system!:", err)
		panic(err)
	}

	conf := logger.Configurations{
		LoggingApplication: Config.LOGGING.LoggingApplication,
		LoggingConsole:     Config.LOGGING.LoggingConsole,
		LoggingErrors:      Config.LOGGING.LoggingErrors,
		LoggingMessaging:   Config.LOGGING.LoggingMessaging,
		MaxFileSize:        999,
		TimeLifeDays:       3,
		NotWriteToFile:     false,
		LoggingErrorsBD:    true,
	}
	logger.SetConfigurations(conf)

}
