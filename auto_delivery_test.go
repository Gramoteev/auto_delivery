package main

import (
	"auto_delivery/action"
	"auto_delivery/geocoder"
	"auto_delivery/postgresql"
	"auto_delivery/redis"
	"fmt"
	"testing"
	"time"

	"auto_delivery/controller"
	"ff_system/logger"
)

func TestInit(T *testing.T) {

	controller.InitController()
}

func TestSelectOrder(T *testing.T) {
	if false {
		tx, _ := postgresql.Begin()
		defer tx.Rollback()
		orders := action.Orders{}

		err := orders.Select(tx, "OrderID2", 556)
		if err != nil {
			panic(err)
		}
		fmt.Printf("%+v", orders)
		tx.Commit()
	}

	if false {

		cc := action.NewCouriers()
		couriers, err := cc.SelectAll(nil, "AllToThePoint", "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44")
		logger.Messaging.Println(err)
		if err != nil {
			if panicCheck {
				panic(err)
			}
		}
		fmt.Printf("%+v", couriers)
	}

}

func TestCourierSet(T *testing.T) {

	if AddCourier {

		//Добавление курьера
		cur := action.NewCouriers()
		cur.UserName = "Никита"
		cur.UserHash = "3"
		cur.Phone = "79091452367"
		cur.PointHash = "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44"
		cur.SurName = "SurName1"
		cur.FirstName = "FirstName1"
		cur.SecondName = "SecondName1"

		if err := cur.Set(); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}

		// //Подвердлил что готов
		// cur.Restaurant = true
		// if err := cur.UpdatePrioritys(); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		//Добавление курьера
		cur2 := action.NewCouriers()
		cur2.UserName = "Матвей"
		cur2.UserHash = "4"
		cur2.Phone = "123456123445"
		cur2.PointHash = "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44"
		cur2.SurName = "SurName2"
		cur2.FirstName = "FirstName2"
		cur2.SecondName = "SecondName2"

		if err := cur2.Set(); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
		//Подвердлил что готов
		// cur2.Restaurant = true
		// if err := cur2.UpdatePrioritys(); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// //Добавим пачку заказов
		// if err := cur.SetOrders("1", []int64{881, 882}); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// //Добавим пачку заказов
		// if err := cur2.SetOrders("2", []int64{995, 996}); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// if err := cur.SetOrderDelivered(nil, 881); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// if err := cur.SetOrderDelivered(nil, 996); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// if err := cur.SetOrderDelivered(nil, 882); err != nil {
		// 	logger.Messaging.Println(err)
		// }

		// if err := cur.SetOrderDelivered(nil, 995); err != nil {
		// 	logger.Messaging.Println(err)
		// }

	}

}

func TestGeocoder(T *testing.T) {

	if false {

		//--
		{
			orders := action.Orders{}
			err := orders.Select(nil, "OrderID", 556)
			if err != nil {
				logger.Messaging.Println(err)
			}
			point, ok := redis.Redis.PointStorage(orders.PointHash)
			if !ok {
				fmt.Printf("\nПроизошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, orders.PointHash)
			}

			geocod := geocoder.NewGeocoder()
			if err := geocod.GetData(orders, point); err != nil {
				logger.Messaging.Println(err)
			}
			geocod.SetDB(nil)
			fmt.Printf("коля мяготина 66 %+v", geocod)
		}
		//--

		{
			orders := action.Orders{}
			err := orders.Select(nil, "OrderID", 557)
			if err != nil {
				logger.Messaging.Println(err)
			}
			point, ok := redis.Redis.PointStorage(orders.PointHash)
			if !ok {
				fmt.Printf("\nПроизошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, orders.PointHash)
			}

			geocod := geocoder.NewGeocoder()
			if err := geocod.GetData(orders, point); err != nil {
				logger.Messaging.Println(err)
			}
			geocod.SetDB(nil)
			fmt.Printf("где то снизу %+v", geocod)
		}

		{
			orders := action.Orders{}
			err := orders.Select(nil, "OrderID", 558)
			if err != nil {
				logger.Messaging.Println(err)
			}
			point, ok := redis.Redis.PointStorage(orders.PointHash)
			if !ok {
				fmt.Printf("\nПроизошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, orders.PointHash)
			}
			geocod := geocoder.NewGeocoder()
			if err := geocod.GetData(orders, point); err != nil {
				logger.Messaging.Println(err)
			}
			geocod.SetDB(nil)
			fmt.Printf("Энергетики %+v", geocod)
		}
		{
			orders := action.Orders{}
			err := orders.Select(nil, "OrderID", 559)
			if err != nil {
				logger.Messaging.Println(err)
			}
			point, ok := redis.Redis.PointStorage(orders.PointHash)
			if !ok {
				fmt.Printf("\nПроизошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, orders.PointHash)
			}
			geocod := geocoder.NewGeocoder()
			if err := geocod.GetData(orders, point); err != nil {
				logger.Messaging.Println(err)
			}
			geocod.SetDB(nil)
			fmt.Printf("Зазик %+v", geocod)
		}

		{
			orders := action.Orders{}
			err := orders.Select(nil, "OrderID", 560)
			if err != nil {
				logger.Messaging.Println(err)
			}
			point, ok := redis.Redis.PointStorage(orders.PointHash)
			if !ok {
				fmt.Printf("\nПроизошла ошибка при получении информации о точке [Geocoder->GetData]: %t, точка %s", ok, orders.PointHash)
			}
			geocod := geocoder.NewGeocoder()
			if err := geocod.GetData(orders, point); err != nil {
				logger.Messaging.Println(err)
			}
			geocod.SetDB(nil)
			fmt.Printf("рябково %+v", geocod)
		}
	}

}

func TestAddNewOrdersController(T *testing.T) {

	if false {

		{
			orders := action.Orders{ //где то снизу
				OrderID:      999,
				PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
				StatusID:     9,
				TimeDelivery: time.Now(),
				Preorder:     true,
				Lat:          "55.436132",
				Lon:          "65.313459",
				Address:      []byte("Коли Мяготина 56"),
			}

			if err := controller.AddOrder(orders); err != nil {
				logger.Messaging.Println(err)
			}
			fmt.Printf(" %+v", 1)

		}

		{
			orders := action.Orders{ // коля мяготина 66
				OrderID:      556,
				PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
				StatusID:     9,
				TimeDelivery: time.Now(),
				Preorder:     true,
				Lat:          "55.441101",
				Lon:          "65.367924",
				Address:      []byte("Кравченко 26"),
			}

			if err := controller.AddOrder(orders); err != nil {
				logger.Messaging.Println(err)
			}
			fmt.Printf(" %+v", 1)

		}

		// {
		// 	orders := action.Orders{ //где то снизу
		// 		OrderID:      557,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     9,
		// 		TimeDelivery: time.Now(),
		// 		Preorder:     true,
		// 		Lat:          "55.416301",
		// 		Lon:          "65.338475",
		// 		//Address:      "СТ ДЕУ слева снизу за городом, в сторону Кетово",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 	}
		// 	fmt.Printf(" %+v", 1)
		// }
		// {
		// 	orders := action.Orders{ //Энергетики
		// 		OrderID:      558,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     9,
		// 		TimeDelivery: time.Now(),
		// 		Preorder:     true,
		// 		Lat:          "55.417601",
		// 		Lon:          "65.244905",
		// 		//Address:      "проспект Конституции, 39Ас1, энергетики",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 	}
		// 	fmt.Printf(" %+v", 1)
		// }
		// {
		// 	orders := action.Orders{ //Зазик
		// 		OrderID:      559,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     9,
		// 		TimeDelivery: time.Now(),
		// 		Preorder:     true,
		// 		Lat:          "55.463013",
		// 		Lon:          "65.278364",
		// 		//Address:      "5-й микрорайон, 18к2",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 	}
		// 	fmt.Printf(" %+v", 1)
		// }
		// {
		// 	orders := action.Orders{ //рябково
		// 		OrderID:      560,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     9,
		// 		TimeDelivery: time.Now(),
		// 		Preorder:     true,
		// 		Lat:          "55.501419",
		// 		Lon:          "65.339221",
		// 		//Address:      "улица Куприна, 32,Рябкого",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 	}
		// 	fmt.Printf(" %+v", 1)
		// }
	}
}

func TestUpStatusOrderController(T *testing.T) {

	if AddOrder {
		{
			orders := action.Orders{
				OrderID:      1,
				PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
				StatusID:     8,
				TimeCooked:   time.Now(),
				TimeDelivery: time.Now().Add(time.Hour), //.UTC(), //.Add(time.Hour)
				Preorder:     true,
				Lat:          "55.441101",
				Lon:          "65.367924",
				Address:      []byte("Кравченко 26"),
			}
			if err := controller.AddOrder(orders); err != nil {
				logger.Messaging.Println(err)
				if panicCheck {
					panic(err)
				}
			}
		}
		{
			orders := action.Orders{
				OrderID:      2,
				PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
				StatusID:     8,
				TimeCooked:   time.Now(),
				TimeDelivery: time.Now().Add(time.Hour), //.UTC(),
				Preorder:     true,
				Lat:          "55.436132",
				Lon:          "65.313459",
				Address:      []byte("Коли Мяготина 56"),
			}
			if err := controller.AddOrder(orders); err != nil {
				logger.Messaging.Println(err)
				if panicCheck {
					panic(err)
				}
			}
		}
		// {
		// 	orders := action.Orders{
		// 		OrderID:      4,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     8,
		// 		TimeCooked:   time.Now().Add(time.Hour),
		// 		TimeDelivery: time.Now(), //.UTC(),
		// 		Preorder:     true,
		// 		Lat:          "55.450267",
		// 		Lon:          "65.356415",
		// 		//Address:      "Бурова петрова Мост, справа сверху",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 		if panicCheck {
		// 			panic(err)
		// 		}
		// 	}
		// }
		// {
		// 	orders := action.Orders{
		// 		OrderID:      5,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     8,
		// 		TimeCooked:   time.Now().Add(time.Hour),
		// 		TimeDelivery: time.Now(), //.UTC(),
		// 		Preorder:     true,
		// 		Lat:          "55.442231",
		// 		Lon:          "65.354605",
		// 		//Address:      "Звездный, справа снизу",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 		if panicCheck {
		// 			panic(err)
		// 		}
		// 	}
		// }

		// {
		// 	orders := action.Orders{
		// 		OrderID:      6,
		// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
		// 		StatusID:     8,
		// 		TimeCooked:   time.Now().Add(time.Hour),
		// 		TimeDelivery: time.Now(), //.UTC(),
		// 		Preorder:     true,
		// 		Lat:          "55.446223",
		// 		Lon:          "65.335818",
		// 		//Address:      "Пригородный, слева сверху",
		// 	}
		// 	if err := controller.AddOrder(orders); err != nil {
		// 		logger.Messaging.Println(err)
		// 		if panicCheck {
		// 			panic(err)
		// 		}
		// 	}
		// }
	}

	if UpStatusOrder {
		orders := action.Orders{ //рябково
			OrderID:   1,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpStatusOrder2 {
		orders := action.Orders{ //рябково
			OrderID:   2,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpdatePrioritys3 {
		cur := action.NewCouriers()
		cur.UserHash = "1"
		cur.Restaurant = true
		if err := controller.SetCourierQueue(*cur); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
		cur.UserHash = "2"
		cur.Restaurant = true
		if err := controller.SetCourierQueue(*cur); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if SetOrderDelivered1 {
		cur := action.NewCouriers()
		if err := cur.SetOrderDelivered(nil, 6); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}
	if SetOrderDelivered2 {
		cur := action.NewCouriers()
		if err := cur.SetOrderDelivered(nil, 4); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}

	}

	if UpStatusOrder3 {
		orders := action.Orders{ //рябково
			OrderID:   2,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpStatusOrder4 {
		orders := action.Orders{ //рябково
			OrderID:   1,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpStatusOrder5 {
		orders := action.Orders{ //рябково
			OrderID:   5,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpStatusOrder6 {

		time.Sleep(time.Second * 3)

		logger.Messaging.Println("-- UpStatusOrder6 --")
		orders := action.Orders{ //рябково
			OrderID:   3,
			PointHash: "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:  9,
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if SetCourierQueue {
		cur := action.NewCouriers()
		cur.UserHash = "1"
		cur.Restaurant = true
		if err := controller.SetCourierQueue(*cur); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	//SetCourierQueue

	if ManagerIntervenes {
		order := action.Orders{OrderID: 616728}
		err := controller.ManagerIntervenes(nil, &order)
		if err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if UpStatusOrder21 {
		orders := action.Orders{ //рябково
			OrderID:    616828,
			PointHash:  "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
			StatusID:   21,
			TimeCooked: time.Now().Add(time.Hour),
		}
		if err := controller.UpStatusOrder(&orders); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}
	}

	if AssignmentOfOrdersToTheCourier {

		tx, _ := postgresql.Begin()
		defer tx.Rollback()

		if err := controller.AssignmentOfOrdersToTheCourier(tx, "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44", controller.Params{EventCourier: true, CollectionOfBundlesAwaitingDelivery: true}); err != nil {
			logger.Messaging.Println(err)
			if panicCheck {
				panic(err)
			}
		}

		tx.Commit()
	}

}

//true false
var panicCheck = true

var AddCourier bool = false
var AddOrder bool = false
var UpStatusOrder bool = false
var UpStatusOrder2 bool = false
var SetOrderDelivered1 bool = false
var SetOrderDelivered2 bool = false
var UpdatePrioritys3 bool = false

var UpStatusOrder3 bool = false
var UpStatusOrder4 bool = false
var UpStatusOrder5 bool = false
var UpStatusOrder6 bool = false
var SetCourierQueue bool = false
var ManagerIntervenes bool = false

var AssignmentOfOrdersToTheCourier bool = true
var UpStatusOrder21 bool = false

//Тест на отмену заказа

// func TestCancelOrderController(T *testing.T) {
// 	orders := action.Orders{
// 		OrderID:      1,
// 		PointHash:    "c4bf0aef0e166a32f899b98841655ea7be08f8c1dd59a28ca28aa86abb92260ce73724f5c3a38a1369479281ff92abc155ca724b9f7b774f11ce8bd81a9a9b44",
// 		StatusID:     8,
// 		TimeCooked:   time.Now().Add(time.Hour),
// 		TimeDelivery: time.Now(), //.UTC(), //.Add(time.Hour)
// 		Preorder:     true,
// 		Lat:          "55.436061",
// 		Lon:          "65.315187",
// 		Address:      "Дом быта слева  снизу",
// 	}
// 	if err := controller.AddOrder(orders); err != nil {
// 		logger.Messaging.Println(err)
// 		if panicCheck {
// 			panic(err)
// 		}
// 	}
// }
