package postgresql

import (
	"database/sql"
	"errors"
	"ff_system/logger"
	"fmt"
	"unicode"

	"github.com/jmoiron/sqlx"

	Config "auto_delivery/config"
)

var requestList map[string]*sqlx.Stmt
var db *sqlx.DB

//---- Транзакции ----//
func Begin() (*sqlx.Tx, error) {
	return db.Beginx()
}

func TxExecReturnID(tx *sqlx.Tx, query, table, typeParameter string, args ...interface{}) (rezult interface{}, err error) {
	var nameRequest = query + "." + table + "." + typeParameter

	stmt, ok := requestList[nameRequest]
	if !ok {
		return nil, errors.New("Missmatch request:" + nameRequest)
	}

	logger.Messaging.Println("TxExecReturnID:", nameRequest, args)
	err = tx.Stmt(stmt.Stmt).QueryRow(args...).Scan(&rezult)
	return rezult, err
}

func TxExec(tx *sqlx.Tx, query, table, typeParameter string, args ...interface{}) error {
	var nameRequest = query + "." + table + "." + typeParameter

	stmt, ok := requestList[nameRequest]
	if !ok {
		return errors.New("Missmatch request:" + nameRequest)
	}

	_, err := tx.Stmt(stmt.Stmt).Exec(args...)
	return err
}

func addRequest(nameRequest, query string) {
	var err error

	_, ok := requestList[nameRequest]
	if ok {
		fmt.Println("Обнаружен дубликат запроса nameRequest:", nameRequest)
		logger.Errors.Println("Обнаружен дубликат запроса nameRequest::", nameRequest)
		logger.Messaging.Println("Обнаружен дубликат запроса nameRequest::", nameRequest)
		panic("Обнаружен дубликат запроса nameRequest" + nameRequest)
	}

	requestList[nameRequest], err = db.Preparex(query)

	if err != nil {
		fmt.Println("nameRequest:", nameRequest)
		logger.Errors.Println("nameRequest:", nameRequest)
		logger.Messaging.Println("nameRequest:", nameRequest)
		panic(err)
	}
}

func Exec(_inTx *sqlx.Tx, query, table, typeParameter string, args ...interface{}) (result interface{}, err error) {
	var nameRequest = query + "." + table + "." + typeParameter

	stmt, ok := requestList[nameRequest]
	if !ok {
		return result, errors.New("Missmatch request:" + nameRequest)
	}
	logger.Messaging.Println("Exec:", nameRequest, args)

	if _inTx != nil {
		logger.Messaging.Println("Не пустой _inTx [Exec]")
		_, err = _inTx.Stmt(stmt.Stmt).Exec(args...)
	} else {
		logger.Messaging.Println("Пустой _inTx [Exec]")
		_, err = stmt.Exec(args...)
	}

	return result, err
}

// *********************************

// func Get(nameRequest string, structure interface{}, args ...interface{}) (err error) {
// 	if _, ok := requestList[nameRequest]; !ok {
// 		return errors.New("Missmatch request:" + nameRequest)
// 	}

// 	return requestList[nameRequest].Get(structure, args...)
// }

// Получение структуры (выборка из базы одного значения)
// Первым аргументом имя запроса
// Вторым аргументом ссылку на ЭКЗЕМПЛЯР структуры
// А дальше неограниченное количество аргументов для запроса
func Get(nameRequest string, structure interface{}, args ...interface{}) (err error) {
	if _, ok := requestList[nameRequest]; !ok {
		return errors.New("Missmatch request:" + nameRequest)
	}

	logger.Messaging.Println("Get:", nameRequest, args)
	return requestList[nameRequest].Get(structure, args...)
}

// Выборка из базы нескольких значений
// Первым аргументом имя запроса
// Вторым аргументом ссылку на ЭКЗЕМПЛЯР структуры
// А дальше неограниченное количество аргументов для запроса
func Select(_inTx *sqlx.Tx, nameRequest string, structure interface{}, args ...interface{}) (err error) {
	logger.Messaging.Println("Select:", nameRequest, args)
	stmt, ok := requestList[nameRequest]
	if !ok {
		return errors.New("Missmatch request:" + nameRequest)
	}

	if _inTx != nil {
		logger.Messaging.Println("Не пустой _inTx [Select]")
		err = _inTx.Stmtx(stmt.Stmt).Select(structure, args...)
	} else {
		logger.Messaging.Println("Пустой _inTx [Select]")
		err = stmt.Select(structure, args...)

	}

	return
}

// Выборка из базы нескольких значений
// Первым аргументом имя запроса
// Вторым аргументом ссылку на ЭКЗЕМПЛЯР структуры
// А дальше неограниченное количество аргументов для запроса
func Query(_inTx *sqlx.Tx, nameRequest string, args ...interface{}) (rows *sql.Rows, err error) {
	stmt, ok := requestList[nameRequest]
	if !ok {
		return nil, errors.New("Missmatch request:" + nameRequest)
	}

	logger.Messaging.Println("Query:", nameRequest, args)
	if _inTx != nil {
		logger.Messaging.Println("Не пустой _inTx [Query]")
		rows, err = _inTx.Stmt(stmt.Stmt).Query(args...)
	} else {
		logger.Messaging.Println("Пустой _inTx [Query]")
		rows, err = stmt.Query(args...)
	}

	return
}

func Check(_inTx *sqlx.Tx, nameRequest string, args ...interface{}) (check bool, err error) {
	stmt, ok := requestList[nameRequest]
	if !ok {
		return false, errors.New("Missmatch request:" + nameRequest)
	}

	logger.Messaging.Println("Check:", nameRequest, args)
	var row *sql.Row
	if _inTx != nil {
		logger.Messaging.Println("Не пустой _inTx [Check]")
		row = _inTx.Stmt(stmt.Stmt).QueryRow(args...)
	} else {
		logger.Messaging.Println("Пустой _inTx [Check]")
		row = requestList[nameRequest].QueryRow(args...)
	}

	// row := requestList[nameRequest].QueryRow(args...)
	if err := row.Scan(&check); err != nil {
		return false, err
	}

	return check, nil
}

// func Queryx(nameRequest string, structure interface{}, args ...interface{}) error {

// 	if _, ok := requestList[nameRequest]; !ok {
// 		return errors.New("Missmatch request:" + nameRequest)
// 	}
// 	rows, err := requestList[nameRequest].Queryx(args)
// 	if err != nil {
// 		return err
// 	}

// 	Orders2 := Orders2{}
// 	for rows.Next() {
// 		err := rows.StructScan(&Orders2)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	structure = Orders2
// 	return nil
// }

// Выборка из базы
// Не требует ссылки на структуру, ответ возвращает с массивом map[string]interface
// где ключ в такой мапе - это имя столбца
func SelectM(table, typeParameter string, args ...interface{}) (result interface{}, err error) {
	var nameRequest = "Select." + table + "." + typeParameter

	if _, ok := requestList[nameRequest]; !ok {
		return result, errors.New("Missmatch request:" + nameRequest)
	}

	logger.Messaging.Println("Selectm:", nameRequest, args)
	rows, err := requestList[nameRequest].Stmt.Query(args...)

	if err != nil {
		return result, err
	}

	defer rows.Close()

	// получим имена всех столбцов в этом запросе
	rowsColumnNames, err := rows.Columns()

	if err != nil {
		return result, err
	}

	// здесь мы будет хранить уже преобразованные имена столбцов
	var columnNames []string

	// теперь наша задача - это превратить имена типа test_org в TestOrg
	// чисто для своего удобства
	for index := range rowsColumnNames {
		// ставим флаг сразу в true, чтобы первый символ улетел в верхний регистр
		flag := true
		var result string
		for _, symbol := range rowsColumnNames[index] {
			if flag {
				symbol = unicode.ToUpper(symbol)
				flag = false
			}

			if symbol == '_' {
				flag = true
			} else {
				result += string(symbol)
			}
		}

		columnNames = append(columnNames, result)
	}

	// столбцы
	columns := make([]interface{}, len(columnNames))
	// указали на столбцы
	columnPointers := make([]interface{}, len(columnNames))

	for i := 0; i < len(columnNames); i++ {
		columnPointers[i] = &columns[i]
	}

	// это делаем для того, чтобы в итоге получился map
	// где ключ это имя стобца, а значение - значение столбца
	var resultArray []map[string]interface{}

	// Проблема в том, что float64 считываются, как uint8
	for rows.Next() {
		if err := rows.Scan(columnPointers...); err != nil {
			return result, err
		}

		result := make(map[string]interface{})

		for index, value := range columnNames {
			result[value] = columns[index]
		}

		resultArray = append(resultArray, result)
	}

	return resultArray, err
}

func InitDatabase() {
	var err error

	//db, err = sql.Open("postgres", "host="+Config.Config.DB.Host+" port="+Config.Config.DB.Port+" user="+Config.Config.DB.Login+" password="+Config.Config.DB.Pass+" dbname="+Config.Config.DB.DB+" sslmode="+Config.Config.DB.SSL)

	db, err = sqlx.Connect("postgres",
		"postgres://"+Config.Config.DB.DBLogin+":"+Config.Config.DB.DBPass+"@"+Config.Config.DB.DBHost+"/"+Config.Config.DB.DBName+"?sslmode=disable")
	if err != nil {
		panic(err)
	}

	requestList = make(map[string]*sqlx.Stmt)

	// Orders
	{
		addRequest("Insert.Orders.",
			`INSERT INTO 
				orders(order_id, id_day_point, point_hash, status_id, time_cooked, time_delivery, preorder, lat, lon, address, products)
			VALUES 
				($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)`)

		addRequest("Update.Orders.Status",
			`UPDATE 
				orders
			SET 
				status_id=$2
			WHERE
				order_id=$1`)

		addRequest("Update.Orders.OrderID",
			`UPDATE public.orders
				SET status_id=$2, time_cooked=$3
			WHERE  order_id=$1`)

		addRequest("Delete.Orders.",
			`delete from orders where order_id=$1`)

		addRequest("Select.Orders.OrderID",
			`SELECT 
				id, order_id, point_hash, status_id, time_cooked, time_delivery, preorder, lat, lon, user_hash, user_name
			FROM 
			   	public.orders
			where 
				order_id=$1`)

		addRequest("Select.Orders.GroupsID",
			`SELECT DISTINCT
				o.id, o.order_id, o.status_id, time_cooked, time_delivery, o.lat, o.lon, go.side_of_the_world
			FROM 
				order_group_opportunities ogo
					inner join orders o on 
						o.order_id=ogo.order_id  
						and o.point_hash=$1
						and o.order_id != $3
					inner join geocoder_orders go on 
						o.order_id = go.order_id 
						and array_position($2::integer[], go.side_of_the_world::integer) IS NOT NULL
			order by status_id desc
			`)

		// `SELECT DISTINCT
		// 	o.id, o.order_id, o.status_id, time_cooked, time_delivery, o.lat, o.lon, go.side_of_the_world
		// FROM
		// 	orders o
		// 		inner join geocoder_orders go on o.order_id = go.order_id and array_position($2::integer[], go.side_of_the_world::integer) IS NOT NULL
		// where
		// 	point_hash=$1 and o.order_id != $3
		// order by time_cooked asc`

	}

	//Couriers
	{
		addRequest("Insert.Couriers.",
			`INSERT INTO public.couriers(user_hash, user_name, sur_name, first_name, second_name, phone, point_hash)
			VALUES ($1, $2, $3, $4, $5, $6, $7);
		`)

		addRequest("Delete.Couriers.UserHash",
			`delete from Couriers where user_hash=$1
				and pack_orders = 0
			`)

		addRequest("Select.Couriers.CheckFree",
			`select count(*)>0 from couriers where free = true`)

		addRequest("Select.Couriers.AllToThePoint",
			`SELECT c.id, c.user_hash, c.user_name, c.sur_name, c.first_name, c.second_name, c.phone, c.point_hash, c.prioritys, c.free, c.pack_orders, 
				(select count(*)>0 
					from couriers_orders co
						inner join orders o on o.order_id=co.order_id and (o.status_id = 10)
					where co.pack_orders_id=c.pack_orders 
				) as in_ten_status, 
				
				c.auto_delivery
			FROM 
				public.couriers c
			where 
				c.point_hash=$1
			order by c.pack_orders desc, c.free, c.prioritys asc
		`)

		addRequest("Select.Couriers.UserHash",
			`SELECT c.id, c.user_hash, c.user_name, c.sur_name, c.first_name, c.second_name, c.phone, c.point_hash, c.prioritys, c.free, c.pack_orders,
				(select count(*)>0 
				from couriers_orders co
					inner join orders o on o.order_id=co.order_id and (o.status_id = 10)
				 where co.pack_orders_id=c.pack_orders ) as in_ten_status, auto_delivery

				FROM public.couriers c
				
				where c.user_hash=$1 
		`)

		addRequest("Select.Couriers.OrderID",
			`SELECT 
				c.id, c.user_hash, c.user_name, c.sur_name, c.first_name, c.second_name, c.phone, c.point_hash, c.prioritys, c.free, c.pack_orders,
				(select count(*)>0 
					from couriers_orders co
						inner join orders o on o.order_id=co.order_id and (o.status_id = 10)
					where co.pack_orders_id=c.pack_orders ) as in_ten_status, auto_delivery
			FROM 
				couriers_orders co
					inner join couriers_pack_orders cpo on cpo.id = co.pack_orders_id
					inner join couriers c on c.user_hash = cpo.user_hash 
			WHERE
				co.order_id=$1
				and co.active;
	`)

		addRequest("Update.Couriers.PackOrders",
			`UPDATE couriers
				SET prioritys=0,
					free=false, pack_orders=$2
				WHERE user_hash=$1`)

		addRequest("Update.Couriers.AutoDelivery",
			`UPDATE couriers
				SET auto_delivery=$2
				WHERE user_hash=$1`)

		addRequest("Update.Couriers.Prioritys",
			`with point_h as (
				select point_hash as ph from couriers where user_hash=$1
			), priors as (
				select max(prioritys)+1 as prior from couriers where point_hash=(select ph from point_h)
			)			
			UPDATE couriers
				SET prioritys=(select prior from priors),
					free=true, pack_orders=0
				WHERE user_hash=$1`)

		addRequest("Select.Couriers.Queue",
			`SELECT 
					id, user_hash, user_name, sur_name, first_name, second_name, phone, point_hash, prioritys, free, pack_orders, auto_delivery
				FROM 
					couriers
				WHERE 
					point_hash=$1 and free and pack_orders=0 and auto_delivery = true
				order by prioritys asc`)

	}

	//couriers_pack_orders
	{ //Insert.CouriersPackOrders. - выполняет вставку пачки заказов. При назначении.
		addRequest("Insert.CouriersPackOrders.",
			`INSERT INTO 
				couriers_pack_orders(user_hash, active)
			VALUES 
				($1, true) returning id`)

		// addRequest("Update.CouriersPackOrders.",
		// 	`with ins as (
		// 		UPDATE public.couriers_pack_orders
		// 			SET active=false
		// 			WHERE user_hash=$1)
		// 		UPDATE couriers
		// 			SET prioritys=,
		// 				free=true, pack_orders=(select id from ins)
		// 			WHERE user_hash=$1`)

		addRequest("Select.CouriersPackOrders.PackID",
			`SELECT cpo.id, cpo.user_hash, co.order_id, o.id_day_point, cpo.time_now, cpo.active, status_id, COALESCE(products,'{}') as products, address, 0 as priority
			FROM public.couriers_pack_orders cpo
				inner join couriers_orders co on co.pack_orders_id = cpo.id 
				inner join orders o on o.order_id=co.order_id
			where cpo.active=true
			and cpo.id=$1`)

		// addRequest("Select.CouriersPackOrders.OrderID",
		// 	`SELECT cpo.id, cpo.user_hash, co.order_id, cpo.time_now, cpo.active, status_id, COALESCE(products,'{}') as products
		// 	FROM public.couriers_pack_orders cpo
		// 		inner join couriers_orders co on co.pack_orders_id = cpo.id
		// 		inner join orders o on o.order_id=co.order_id and o.order_id=$1
		// 	where cpo.active=true`)

		addRequest("Select.CouriersPackOrders.OrderIDorPackID",
			`SELECT cpo.id, cpo.user_hash, co.order_id, o.id_day_point, cpo.time_now, cpo.active, status_id, COALESCE(products,'{}') as products, 0 as priority
			FROM public.couriers_orders co
			  inner join couriers_pack_orders cpo on co.pack_orders_id = cpo.id 
			  inner join orders o on o.order_id=co.order_id
			  where co.pack_orders_id = (
					  select 
						  pack_orders_id
					  from 
						  couriers_orders 
					  where 
						  order_id=$1);`)

		addRequest("Delete.CouriersPackOrders.PackID",
			`DELETE FROM 
				public.couriers_pack_orders
			WHERE id=$1`)

	}

	//CouriersOrders
	{
		addRequest("Insert.CouriersOrders.",
			`INSERT INTO 
				couriers_orders(pack_orders_id, order_id)
			VALUES 
				($1, $2)`)

		addRequest("Check.CouriersOrders.OrderID",
			`select count(*)>0 from couriers_orders where order_id=$1`)

		//Обновить информацию по доставленному заказу в таблицах паков и курьеров
		addRequest("Update.CouriersOrders.Active",
			`with cpo as (
				select pack_orders_id from couriers_orders where order_id=$1 order by id desc limit 1
			),
			quer as (
				select count(*) as len_active from couriers_orders where pack_orders_id=(select pack_orders_id from cpo) and active and order_id != $1
			),			
			up as (UPDATE
					couriers_orders
				SET
					active=false
				WHERE
					order_id=$1),
			up2 as (UPDATE 
				couriers_pack_orders
			SET 
				active=false
			WHERE 
				id = (select pack_orders_id from cpo) and
				(select len_active from quer) = 0
			), 
			up3 as (
				UPDATE 
					public.orders
				SET 
					status_id=11
				WHERE 
				 	order_id=$1
			)
			UPDATE couriers
			   SET prioritys=0, free=false, pack_orders=0
			 WHERE pack_orders=(select pack_orders_id from cpo)
			 and (select len_active from quer) = 0`)

		//Удаляем заказ, в случае если больше нет активных заказов, закрываем пачку и обнуляем курьера + обнуляем курьера
		addRequest("Rollback.CouriersOrders.OrderID",
			`
			with cpo as (
				select pack_orders_id from couriers_orders where order_id=$1 order by id desc limit 1
			),
			quer as (
				select count(*) as len_active from couriers_orders where pack_orders_id=(select pack_orders_id from cpo) and active and order_id != $1
			),			
			up as (DELETE 
				FROM 
					couriers_orders
				WHERE 
					order_id=$1),
			up2 as (UPDATE 
				couriers_pack_orders
			SET 
				active=false
			WHERE 
				id = (select pack_orders_id from cpo) and
				(select len_active from quer) = 0
			)
			UPDATE couriers
			   SET prioritys=0, free=false, pack_orders=0
			WHERE pack_orders=(select pack_orders_id from cpo)
			and (select len_active from quer) = 0
			`)
	}

	//geocoder_orders
	{
		addRequest("Insert.GeocoderOrders.",
			`INSERT INTO geocoder_orders(
				order_id, disstance, travel_time, avr_travel_time, side_of_the_world)
			VALUES ($1, $2, $3, $4, $5)`)

		addRequest("Select.GeocoderOrders.OrderID",
			`SELECT 
				id, order_id, disstance, travel_time, avr_travel_time, side_of_the_world
			FROM 
				geocoder_orders
			where order_id=$1`)

	}

	//order_group_opportunities, order_group_opportunities_list
	{
		addRequest("Insert.OrderGroupOpportunities.",
			`INSERT INTO order_group_opportunities(
					order_id)
			VALUES ($1) returning id`)

		addRequest("Insert.OrderGroupOpportunitiesList.",
			`INSERT INTO order_group_opportunities_list(
				order_group_opportunities_id, order_id, priority, disstance, travel_time, side_of_the_world)
			VALUES ($1, $2, $3, $4, $5, $6)`)

		addRequest("Insert.OrderGroupOpportunitiesList.OrderIDGroup",
			`INSERT INTO order_group_opportunities_list(
				order_group_opportunities_id, order_id, priority, disstance, travel_time, side_of_the_world)
			VALUES ((select id from order_group_opportunities where order_id=$1), $2, $3, $4, $5, $6)`)

		addRequest("Delete.OrderGroupOpportunities.OrderID",
			`with del as (DELETE FROM order_group_opportunities
				WHERE order_id=$1)
			DELETE FROM order_group_opportunities_list
				WHERE order_id=$1`)

		addRequest("Check.OrderGroupOpportunities.OrderID",
			`SELECT count(*) FROM public.order_group_opportunities where order_id = $1;
		  `)

		addRequest("Select.OrderGroupOpportunitiesList.OrderID",
			`SELECT ogol.id, ogol.order_group_opportunities_id, geos.side_of_the_world, geos.disstance as disstance_base_point, geos.travel_time as travel_time_base_point,  ogol.order_id, o.status_id, o.time_cooked, o.time_delivery, o.preorder, ogol.priority, ogol.disstance , ogol.travel_time
			FROM order_group_opportunities og  
				inner join order_group_opportunities_list ogol on og.id=ogol.order_group_opportunities_id 
				inner join geocoder_orders geos on ogol.order_id=geos.order_id
				inner join orders o on o.order_id=ogol.order_id and status_id <= 9
			where og.order_id = $1
			  order by geos.side_of_the_world desc, ogol.travel_time asc;
		  `)

		addRequest("Select.OrderGroupOpportunitiesList.PackID",
			`SELECT ogol.id, ogol.order_group_opportunities_id, geos.side_of_the_world, geos.disstance as disstance_base_point, geos.travel_time as travel_time_base_point,  ogol.order_id, o.status_id, o.time_cooked, o.time_delivery, o.preorder, ogol.priority, ogol.disstance , ogol.travel_time
		  FROM order_group_opportunities og  
			  inner join order_group_opportunities_list ogol on og.id=ogol.order_group_opportunities_id 
			  inner join geocoder_orders geos on ogol.order_id=geos.order_id
			  inner join orders o on o.order_id=ogol.order_id
		  where og.id = $1
			order by geos.side_of_the_world desc, ogol.travel_time asc;`)

		{

			addRequest("Insert.OrdersAwaitingDelivery.",
				`INSERT INTO orders_awaiting_delivery(
					point_hash, pack, time_insert)
			VALUES ($1, $2, $3)`)

			addRequest("Select.OrdersAwaitingDelivery.LastPack",
				`select pack from orders_awaiting_delivery order by pack desc`)

			addRequest("Select.OrdersAwaitingDelivery.FirstPack",
				`SELECT 
					oadl.order_id, pack, o.status_id, o.id_day_point, o.time_cooked, o.time_delivery,
					geos.side_of_the_world, geos.disstance, geos.travel_time,
					o.lat, o.lon
				FROM orders_awaiting_delivery oad
					inner join orders_awaiting_delivery_list oadl on oad.pack=oadl.pack_id
					inner join orders o on o.order_id=oadl.order_id
					inner join geocoder_orders geos on geos.order_id=oadl.order_id
				  where 
					oad.point_hash=$1 
					and (
						SELECT count(*)
						FROM orders_awaiting_delivery_list oadl
						inner join orders o on o.order_Id=oadl.order_id and o.status_id < 9  
						where oadl.pack_id=pack			
					) = 0
				order by pack asc`)

			addRequest("Select.OrdersAwaitingDelivery.AllPack",
				`SELECT oadl.order_id, pack, o.status_id, o.id_day_point, o.time_cooked
				FROM orders_awaiting_delivery oad
					inner join orders_awaiting_delivery_list oadl on oad.pack=oadl.pack_id
					inner join orders o on o.order_id=oadl.order_id

				  where oad.point_hash=$1
				order by pack asc`)

			addRequest("Select.OrdersAwaitingDelivery.AllPackSoloOrderNotPack",
				`SELECT 
					oadl.order_id, pack, o.status_id, o.id_day_point, o.time_cooked, o.time_delivery,
					geos.side_of_the_world, geos.disstance, geos.travel_time,
					o.lat, o.lon
				FROM orders_awaiting_delivery oad
					inner join orders_awaiting_delivery_list oadl on oad.pack=oadl.pack_id
					inner join orders o on o.order_id=oadl.order_id
					inner join geocoder_orders geos on geos.order_id=oadl.order_id
				where 
					oad.point_hash=$1 
					and	pack_id <> $2 
					and	(select count(*) from orders_awaiting_delivery_list oadlSolo where oadlSolo.pack_id = oad.pack) = 1
				order by pack asc`)

			addRequest("Delete.OrdersAwaitingDelivery.PackID",
				`DELETE FROM orders_awaiting_delivery WHERE pack=$1`)

			addRequest("Delete.OrdersAwaitingDeliveryList.OrderID",
				`DELETE FROM 
						orders_awaiting_delivery_list
					WHERE			   
						 order_id=$1
					`)

			addRequest("Insert.OrdersAwaitingDeliveryList.",
				`INSERT INTO orders_awaiting_delivery_list(
					pack_id, order_id)
			VALUES ($1, $2)`)

			addRequest("Check.OrdersAwaitingDeliveryList.OrderID",
				`select count(*)>0 from orders_awaiting_delivery_list where order_id=$1`)

			addRequest("Select.OrdersAwaitingDeliveryList.OrderID",
				`select 
					pack_id, order_id
				from 
					orders_awaiting_delivery_list 
				where 
					pack_id=(select 
						pack_id
					from 
						orders_awaiting_delivery_list 
					where 
						order_id=$1 limit 1)
				`)

		}
		/*
			// 	`with Or as (
			// 		select pack  as or_pack
			// 		from orders_awaiting_delivery
			// 		where point_hash=$1
			// 		order by pack asc
			// 	), quer as (
			// 		select * from Or where (select count(*) from orders_awaiting_delivery where pack=or_pack)=0
			// 	)
		*/

	}

	for key, _ := range requestList {
		logger.Messaging.Println("Инициализирован запрос:" + key)
	}

	//sqlx
	logger.Messaging.Println(db)

}
