package postgresql

import (
	structures "auto_delivery/structures"
	"errors"
	"ff_system/connect/structExchange"
)

var UsingGlobal global

type global struct {
	map_select_struct     map[string]interface{}
	exclude_map_select    map[string]map[string]bool
	req_values_map_select map[string]func([]string, structExchange.RTable) []interface{}
}

//создание и заполнение структуры UsingGlobal
func init() {
	UsingGlobal.map_select_struct = make(map[string]interface{})
	UsingGlobal.exclude_map_select = make(map[string]map[string]bool)
	UsingGlobal.req_values_map_select = make(map[string]func([]string, structExchange.RTable) []interface{})
	UsingGlobal.init_obj_map_select_struct()
	UsingGlobal.init_exclude_map_select()
	UsingGlobal.init_req_values_map_select()
}

//Заполнение поля map_select_struct позволяющая менять структуры возвращаемые разными запросами
func (self *global) init_obj_map_select_struct() {
	//Для микросервиса "Табель"
	UsingGlobal.map_select_struct["SessionTabel"] = (*structures.SessionTabelRenurn)(nil)
	//Для микросервиса "Заказ"
	UsingGlobal.map_select_struct["PointRoleCount"] = (*structures.PointRoleCountRenurn)(nil)
	//Для микросервиса "Заказ" SessionInfo
	UsingGlobal.map_select_struct["SessionInfo"] = (*structures.SessionInfoRenurn)(nil)
	//Для микросервиса "Админ"
	UsingGlobal.map_select_struct["Rights"] = (*structures.RightsRenurn)(nil)
	//Одиночное чтение
	UsingGlobal.map_select_struct["Session"] = (*structures.SessionRenurn)(nil)
	// Pauses
	UsingGlobal.map_select_struct["Pauses"] = (*structures.PausesRenurn)(nil)
	// Causes
	UsingGlobal.map_select_struct["Causes"] = (*structures.CausesRenurn)(nil)
	//SessionActive
	UsingGlobal.map_select_struct["SessionActive"] = (*structures.SessionActiveRenurn)(nil)
	UsingGlobal.map_select_struct["SessionDataLog"] = (*structures.SessionDataLogRenurn)(nil)
}

//Заполнение поля exclude_map_select - содержит мапы полей которые не участвуют при селектах
func (self *global) init_exclude_map_select() {
	UsingGlobal.exclude_map_select["SessionTabel."] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.UserHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.Date"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.UserHashDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.Off"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OffUserHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OffDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OffUserHashDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.On"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OnUserHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OnDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionTabel.OnUserHashDate"] = map[string]bool{}
	//Для микросервиса "Заказ"
	UsingGlobal.exclude_map_select["PointRoleCount."] = map[string]bool{}
	//Для микросервиса "Заказ" SessionInfo
	UsingGlobal.exclude_map_select["SessionInfo.OnOrganizationHashRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionInfo.Hash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionInfo.OnUserHash"] = map[string]bool{}
	//Для микросервиса "Админ"
	UsingGlobal.exclude_map_select["Rights.Hash"] = map[string]bool{}
	//Одиночное чтение
	UsingGlobal.exclude_map_select["Session.Hash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Session.OnOneUserHash"] = map[string]bool{}
	//Множественное чтение, все сессии(активные и законченные)
	UsingGlobal.exclude_map_select["Session.UserActivAll"] = map[string]bool{"SessionHash": true, "PhoneNumber": true,
		"SurName": true, "FirstName": true, "SecondName": true, "VPNNumber": true, "VPNPassword": true, "Language": true, "RoleHash": true, "RoleName": true, "OrganizationHash": true,
		"OrganizationName": true, "Rights": true, "SkladName": true, "SessionData": true, "Begin": true, "End": true, "Stage": true, "Level": true}
	UsingGlobal.exclude_map_select["Session.UserHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.RoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.Begin"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.Begin>"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.DateRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.DateOrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.DateOrganizationHashRoleHash"] = map[string]bool{"Level": true}
	//Множественное чтение, активные сессии On
	UsingGlobal.exclude_map_select["Session.OnUserHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnOrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnBegin"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnOrganizationHashRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnDateRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnDateOrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OnDateOrganizationHashRoleHash"] = map[string]bool{"Level": true}
	//Множественное чтение, законченные сессии Off
	UsingGlobal.exclude_map_select["Session.OffUserHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffOrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffBegin"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffDateRoleHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffDateOrganizationHash"] = map[string]bool{"Level": true}
	UsingGlobal.exclude_map_select["Session.OffDateOrganizationHashRoleHash"] = map[string]bool{"Level": true}
	// Pauses
	UsingGlobal.exclude_map_select["Pauses.ID"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.Date"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OnDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OffDate"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.DateRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OnDateRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OffDateRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.DateOrganizationHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OnDateOrganizationHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OffDateOrganizationHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.DateOrganizationHashRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OnDateOrganizationHashRoleHash"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Pauses.OffDateOrganizationHashRoleHash"] = map[string]bool{}
	//Causes
	UsingGlobal.exclude_map_select["Causes.ID"] = map[string]bool{}
	UsingGlobal.exclude_map_select["Causes."] = map[string]bool{}
	//SessionActive.
	UsingGlobal.exclude_map_select["SessionActive."] = map[string]bool{}
	//SessionDataLog
	UsingGlobal.exclude_map_select["SessionDataLog.Min"] = map[string]bool{}
	UsingGlobal.exclude_map_select["SessionDataLog.Between"] = map[string]bool{"Age": true}
	UsingGlobal.exclude_map_select["SessionDataLog.OrderID"] = map[string]bool{"Age": true}
	UsingGlobal.exclude_map_select["SessionDataLog.UserHash"] = map[string]bool{"Age": true}

}

//Заполнение поля req_values_map_selec - содержит интерфейсы переменных которые ожитаются от заказчика
func (self *global) init_req_values_map_select() {
	//Для микросервиса "Табель"
	UsingGlobal.req_values_map_select["SessionTabel."] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.UserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.Date"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.UserHashDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.Off"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OffUserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OffDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OffUserHashDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.On"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OnUserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OnDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionTabel.OnUserHashDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	//Для микросервиса "Заказ"
	UsingGlobal.req_values_map_select["PointRoleCount."] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{} }
	//Для микросервиса "Заказ" SessionInfo
	UsingGlobal.req_values_map_select["SessionInfo.OnOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionInfo.Hash"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	UsingGlobal.req_values_map_select["SessionInfo.OnUserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	//Для микросервиса "Админ"
	UsingGlobal.req_values_map_select["Rights.Hash"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	//Одиночное чтение
	UsingGlobal.req_values_map_select["Session.Hash"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	UsingGlobal.req_values_map_select["Session.OnOneUserHash"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	//Множественное чтение, все сессии(активные и законченные)
	UsingGlobal.req_values_map_select["Session.UserActivAll"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{} }
	UsingGlobal.req_values_map_select["Session.UserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.RoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.Begin"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.Begin>"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.DateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.DateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.DateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	//Множественное чтение, активные сессии On
	UsingGlobal.req_values_map_select["Session.OnUserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnBegin"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnDateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnDateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OnDateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	//Множественное чтение, законченные сессии Off
	UsingGlobal.req_values_map_select["Session.OffUserHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffBegin"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffDateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffDateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Session.OffDateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	// Pauses
	UsingGlobal.req_values_map_select["Pauses.ID"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	UsingGlobal.req_values_map_select["Pauses.Date"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OnDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OffDate"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.DateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OnDateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OffDateRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.DateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OnDateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OffDateOrganizationHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.DateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OnDateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["Pauses.OffDateOrganizationHashRoleHash"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1], values[2], table.Limit, table.Offset}
	}
	//Causes
	UsingGlobal.req_values_map_select["Causes.ID"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	UsingGlobal.req_values_map_select["Causes."] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{table.Limit, table.Offset}
	}
	//SessionActive.
	UsingGlobal.req_values_map_select["SessionActive."] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{table.Limit, table.Offset}
	}
	UsingGlobal.req_values_map_select["SessionDataLog.Min"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1]}
	}
	UsingGlobal.req_values_map_select["SessionDataLog.Between"] = func(values []string, table structExchange.RTable) []interface{} {
		return []interface{}{values[0], values[1]}
	}
	UsingGlobal.req_values_map_select["SessionDataLog.OrderID"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }
	UsingGlobal.req_values_map_select["SessionDataLog.UserHash"] = func(values []string, table structExchange.RTable) []interface{} { return []interface{}{values[0]} }

}

//функция получения конкретной структуры
func (self *global) GetObjMapSelectStruct(inName string) (obj interface{}, err error) {
	var ok bool
	obj, ok = UsingGlobal.map_select_struct[inName]
	if !ok {
		return obj, errors.New("Объект выборки не найден [GetObjMapSelectStruct]")
	}
	return
}

//функция получения конкретного мапа с исключениями
func (self *global) GetObjExcludeMapSelect(inName string) (obj map[string]bool, err error) {
	var ok bool
	obj, ok = UsingGlobal.exclude_map_select[inName]
	if !ok {
		return obj, errors.New("Объект выборки не найден [GetObjExcludeMapSelect]")
	}
	return
}

// функция возвращает конкретный интерфейс который будет заполнен переменными поступимвшими при запросе
func (self *global) GetObjReqValuesMapSelect(inName string) (obj func([]string, structExchange.RTable) []interface{}, err error) {
	var ok bool
	obj, ok = UsingGlobal.req_values_map_select[inName]
	if !ok {
		return obj, errors.New("Объект выборки не найден [GetObjReqValuesMapSelect]")
	}
	return
}
