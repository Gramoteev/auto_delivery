package main

import (
	"auto_delivery/action"
	"auto_delivery/config"
	"auto_delivery/controller"
	"auto_delivery/postgresql"
	"encoding/json"
	"ff_system/connect/structExchange"
	"ff_system/logger"
	"ff_system/servisconfig"
	"fmt"
)

type working struct{}

func NewWorking() *working {
	return new(working)
}

func (self *working) Action(_inMessage *structExchange.RMessage) (_outTable []structExchange.WTable, _outError structExchange.Error) {

	switch _inMessage.Query {
	case "Select":
		{
			for _, table := range _inMessage.Tables {
				//в values у нас тупо строки хэшкодов

				wtable := table.CopyToW()

				var wvalues interface{}
				switch table.Name {
				case "Courier": // Получить список курьеров в очереди
					var pointHash []string
					err := json.Unmarshal(table.Values, &pointHash)
					if err != nil {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
					}
					if len(pointHash) > 0 {
						var cc action.Courier
						wvalues, err = cc.SelectAll(nil, "AllToThePoint", pointHash[0])
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}
					} else {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: "Вы передали пустые параметры в запросе"}
					}

				case "CouriersPackOrders":
					var PackID []int64
					err := json.Unmarshal(table.Values, &PackID)
					if err != nil {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
					}

					if len(PackID) == 0 {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: "len(PackID)  == 0"}
					}

					if PackID[0] == 0 {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: "PackID == 0"}
					}

					cpo := action.CouriersPackOrders{}
					wvalues, err = cpo.Query(nil, "PackID", PackID[0])
					if err != nil {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
					}
				case "OrdersAwaitingDelivery": // Получить список курьеров в очереди
					var pointHash []string
					err := json.Unmarshal(table.Values, &pointHash)
					if err != nil {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
					}
					if len(pointHash) > 0 {
						wvalues, err = action.NewOrdersAwaitingDelivery().Query(nil, "AllPack", pointHash[0])
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}
					} else {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: "Вы передали пустые параметры в запросе"}
					}

				default:
					return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: _inMessage.Query + "." + table.Name + "." + table.TypeParameter + " not implemented"}
				}

				logger.Messaging.Println(wvalues)
				wtable.Values = wvalues
				_outTable = append(_outTable, wtable)
			} //for
		} //Select
	case "Insert": // вставка данных в базу данных
		{
			//var requests = []postgresql.Request{}
			for _, table := range _inMessage.Tables {

				wtable := table.CopyToW()

				switch table.Name {
				case "Courier": // Основная таблица

					switch table.TypeParameter {
					case "Set":
						var arrayStuctures []action.Courier
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Postgre: Insert in Postgre", fmt.Sprintf("%+v", arrayStuctures))
							err = controller.SetNewCourier(S)
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}
					default:
						return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: _inMessage.Query + "." + table.Name + "." + table.TypeParameter + " not implemented"}
					}

				default:
					return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: "cannot insert"}
				}
			}

		} //Insert
	case "Update": // вставка данных в базу данных
		{
			//var requests = []postgresql.Request{}
			for _, table := range _inMessage.Tables {

				wtable := table.CopyToW()

				switch table.Name {
				case "Config":
					switch table.TypeParameter {
					case "":
						var path []interface{}
						err := json.Unmarshal(table.Values, &path)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}
						wtable := table.CopyToW()
						if len(path) == 0 {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: "Путь к ini не получен"}
						}
						//обновляем конфигурацию из удаленного ini файла
						//TODO Нужно менять при копировании
						tempConfig, err := servisconfig.UpdateConfigRemote(fmt.Sprint(path[0]), config.Location, config.FileName, []string{"AutoDelivery"}, config.Config)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.ValuesValueError, Description: err.Error()}
						}
						config.ConfigMutex.Lock()
						config.Config = tempConfig.(config.Configurations)
						config.ConfigMutex.Unlock()
						wtable.Values = true
						_outTable = append(_outTable, wtable)
					case "Get":
						wtable := table.CopyToW()
						wtable.Values = []interface{}{config.Config}
						_outTable = append(_outTable, wtable)
					}
				case "Courier": //Ставим курьера в очередь

					switch table.TypeParameter {
					case "Prioritys":

						var arrayStuctures []action.Courier
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Postgre: Update in Postgre", fmt.Sprintf("%+v", arrayStuctures))
							err = S.UpdatePrioritys()
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}

					case "AutoDelivery":
						var arrayStuctures []action.Courier
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Postgre: Update in Postgre", fmt.Sprintf("%+v", arrayStuctures))
							err = controller.SetCourierAutoDelivery(S)
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}

							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}

					default:
						return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: _inMessage.Query + "." + table.Name + "." + table.TypeParameter + " not implemented"}
					}

				default:
					return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: "cannot insert"}
				}
			}

		} //Update
	case "Delete": // вставка данных в базу данных
		{
			//var requests = []postgresql.Request{}
			for _, table := range _inMessage.Tables {

				wtable := table.CopyToW()

				switch table.Name {
				case "Courier": // Основная таблица

					switch table.TypeParameter {
					case "Delete":

						var arrayStuctures []action.Courier
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Postgre: Delete in Postgre", fmt.Sprintf("%+v", arrayStuctures))
							err = S.Delete()
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}

					default:
						return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: _inMessage.Query + "." + table.Name + "." + table.TypeParameter + " not implemented"}
					}

				default:
					return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: "cannot insert"}
				}
			}

		} //Delete
	case "Methods": // вставка данных в базу данных
		{
			//var requests = []postgresql.Request{}
			for _, table := range _inMessage.Tables {

				wtable := table.CopyToW()

				switch table.Name {
				case "AddOrder": // Добавление заказа в модуль

					var arrayStuctures []action.Orders
					err := json.Unmarshal(table.Values, &arrayStuctures)
					if err != nil {
						return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
					}

					for _, S := range arrayStuctures {
						logger.Messaging.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))
						err = controller.AddOrder(S)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
						}
						_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
					}
				case "UpStatusOrder": // Обновляет статус заказа
					{
						var arrayStuctures []action.Orders
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))
							err = controller.UpStatusOrder(&S)
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}
					}
				case "ManagerIntervenes": // Вмешивается менеджер и ставит заказа сам
					{
						var arrayStuctures []action.Orders
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						tx, err := postgresql.Begin()
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
						}
						defer tx.Rollback()

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))

							err = controller.ManagerIntervenes(tx, &S)
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}

						err = tx.Commit()
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
						}
					}
				case "SetCourierQueue": // Делаем курьера активным
					{
						var arrayStuctures []action.Courier
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						for _, S := range arrayStuctures {
							logger.Application.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))
							err = controller.SetCourierQueue(S)
							if err != nil {
								//logger.Errors.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))
								//return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface(S.UserHash))
						}
					}
				case "AssignmentOfOrdersToTheCourier": // Принудительно запускаем назначение
					{
						var arrayStuctures []string
						err := json.Unmarshal(table.Values, &arrayStuctures)
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.UnmarshalValues, Description: err.Error()}
						}

						tx, err := postgresql.Begin()
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
						}
						defer tx.Rollback()

						for _, S := range arrayStuctures {
							logger.Messaging.Println("Methods:", fmt.Sprintf("%+v", arrayStuctures))
							err = controller.AssignmentOfOrdersToTheCourier(tx, S, controller.Params{EventCourier: true, CollectionOfBundlesAwaitingDelivery: true})
							if err != nil {
								return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
							}
							_outTable = append(_outTable, *wtable.CopySliceToInterface("ok"))
						}

						err = tx.Commit()
						if err != nil {
							return nil, structExchange.Error{Code: structExchange.SQLQueryError, Description: err.Error()}
						}
					}
				default:
					return nil, structExchange.Error{Code: structExchange.TableNameCountError, Description: "cannot insert"}
				}
			}

		} //Methods
	default:
		return nil, structExchange.Error{Code: structExchange.QueryNotImplemented, Description: _inMessage.Query + " not implemented"}
	}

	return
}
