package structures

import (
	"time"

	"github.com/lib/pq"
)

//----------- Старые структурки-------------------

type Tabel struct {
	User  string
	Begin time.Time
	End   time.Time
}

type SessionInfo struct {
	SessionHash string
	UserHash    string
	SurName     string
	FirstName   string
	SecondName  string
	VPNNumber   string
	Language    string
	RoleHash    string
	RoleName    string

	OrganizationHash string
	OrganizationName string

	UserInfoPoints string
	WorkPointHash  string
	WorkPointName  string

	SessionData string
	Stage       int64

	Sort     int64
	Restoran bool
}

type SessionActive struct {
	PointHash   string
	RoleHash    string
	RoleName    string
	FIO         string
	VPNNumber   string
	PhoneNumber string
	Stage       int64
}

type ReadPointRoleCount struct {
	PointHash string
	RoleHash  string
	Count     int64
}

//----------- Старые структурки------------------- end

type SessionTabelRenurn struct {
	User  string
	Begin time.Time
	End   time.Time
}
type PointRoleCountRenurn struct {
	PointHash string
	RoleHash  string
	Count     int64
}

type SessionInfoRenurn struct {
	SessionHash      string
	UserHash         string
	SurName          string
	FirstName        string
	SecondName       string
	VPNNumber        string
	Language         string
	RoleHash         string
	RoleName         string
	OrganizationHash string
	OrganizationName string

	UserInfoPoints string
	WorkPointHash  string
	WorkPointName  string

	SessionData string
	Stage       int64

	Sort     int64
	Restoran bool
}

type RightsRenurn struct {
	Rights string
}

type SessionRenurn struct {
	SessionHash      string
	UserHash         string
	PhoneNumber      string
	SurName          string
	FirstName        string
	SecondName       string
	VPNNumber        string
	VPNPassword      string
	Language         string
	RoleHash         string
	RoleName         string
	OrganizationHash string
	OrganizationName string

	UserInfoPoints string
	WorkPointHash  string
	WorkPointName  string

	Rights      string
	SkladName   pq.StringArray
	SessionData string
	Begin       time.Time
	BeginStr    string
	End         time.Time
	EndStr      string
	Stage       int64
	Level       int64

	Sort     int64
	Restoran bool
}

type PausesRenurn struct {
	Id               int64
	UserHash         string
	Begin            time.Time
	BeginStr         string
	End              time.Time
	EndStr           string
	Cause_id         int64
	Comment          string
	UserName         string
	RoleHash         string
	RoleName         string
	OrganizationHash string
	OrganizationName string
}
type CausesRenurn struct {
	Id      int64
	Name    string
	Minutes int64
}

type SessionActiveRenurn struct {
	PointHash   string
	RoleHash    string
	RoleName    string
	FIO         string
	VPNNumber   string
	PhoneNumber string
	Stage       int64
}

type SessionDataLogRenurn struct {
	Id               int64
	Sessionhash      string
	Date             time.Time
	Sessiondata      string
	Age              float64
	UserHash         string
	PhoneNumber      string
	SurName          string
	FirstName        string
	SecondName       string
	RoleHash         string
	RoleName         string
	OrganizationHash string
	OrganizationName string
	Begin            time.Time
	End              time.Time
}
