drop table orders;
CREATE TABLE orders
(
  id serial,
  order_id integer NOT NULL,
  point_hash character varying NOT NULL,
  status_id integer NOT NULL,
  time_cooked timestamp without time zone not null,
  time_delivery timestamp without time zone NOT NULL,
  preorder boolean NOT NULL,
  lat character varying NOT NULL,
  lon character varying NOT NULL,
  user_hash character varying NOT NULL DEFAULT ''::character varying,
  user_name character varying NOT NULL DEFAULT ''::character varying,
  address character varying,
  assigned boolean NOT NULL DEFAULT false,
  CONSTRAINT orders_pkey PRIMARY KEY (id),
  CONSTRAINT orders_order_id_key UNIQUE (order_id)
)


CREATE TABLE orders_awaiting_delivery
(
  id serial,
  point_hash character varying NOT NULL,
  pack integer NOT NULL,
  time_insert timestamp with time zone NOT NULL,
  CONSTRAINT orders_awaiting_delivery_pkey PRIMARY KEY (id),
  CONSTRAINT orders_awaiting_delivery_pack_key UNIQUE (pack)
)

CREATE TABLE public.orders_awaiting_delivery_list
(
  id serial,
  pack_id integer NOT NULL,
  order_id integer NOT NULL,
  CONSTRAINT orders_awaiting_delivery_list_pkey PRIMARY KEY (id),
  CONSTRAINT orders_awaiting_delivery_list_pack_id_fkey FOREIGN KEY (pack_id)
      REFERENCES public.orders_awaiting_delivery (pack) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)

CREATE TABLE couriers
(
  id serial,
  user_hash varchar not null,
  user_name varchar not null,
  phone varchar not null,
  point_hash varchar not null,
  prioritys integer default 0,
  free boolean default false,
  pack_orders integer default 0,
  CONSTRAINT couriers_user_hash_key UNIQUE (user_hash)
)


CREATE TABLE public.couriers_pack_orders
(
  id serial,
  user_hash character varying NOT NULL,
  time_now timestamp without time zone DEFAULT now(),
  active boolean
)


CREATE TABLE public.couriers_orders
(
  id serial,
  pack_orders_id integer NOT NULL,
  order_id integer NOT NULL,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT couriers_orders_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES public.orders (order_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)


---------------------- Геокодирование
drop table geocoder_orders;
CREATE TABLE public.geocoder_orders
(
  id serial,
  order_id integer NOT NULL,
  disstance integer,
  travel_time integer NOT NULL,
  avr_travel_time integer NOT NULL,
  side_of_the_world integer NOT NULL,
  address character varying,
  CONSTRAINT geocoder_orders_pkey PRIMARY KEY (id),
  CONSTRAINT geocoder_orders_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES public.orders (order_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT geocoder_orders_order_id_key UNIQUE (order_id)
)


--order_group_opportunities - таблица с вариантами для указаного заказа

CREATE TABLE public.order_group_opportunities
(
  id serial,
  order_id integer NOT NULL,
  active boolean NOT NULL DEFAULT true,
  CONSTRAINT order_group_opportunities_pkey PRIMARY KEY (id),
  CONSTRAINT order_group_opportunities_order_id_key UNIQUE (order_id)
)


--order_group_opportunities_list - таблица со списком вариантов для заказа
drop table order_group_opportunities_list;
CREATE TABLE order_group_opportunities_list
(
  id serial,
  order_group_opportunities_id integer NOT NULL,
  order_id integer NOT NULL,
  priority integer NOT NULL,
  disstance integer,
  travel_time integer NOT NULL,
  side_of_the_world character varying NOT NULL,
  CONSTRAINT order_group_opportunities_list_pkey PRIMARY KEY (id),
  CONSTRAINT order_group_opportunities_list_order_id_fkey FOREIGN KEY (order_id)
      REFERENCES public.order_group_opportunities (order_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)




----- допки

DELETE FROM public.orders;
/*
DELETE FROM public.couriers_pack_orders;
DELETE FROM public.couriers;
DELETE FROM public.couriers_orders;



DELETE FROM geocoder_orders;
DELETE FROM order_group_opportunities;
DELETE FROM order_group_opportunities_list;
DELETE FROM orders;

DELETE FROM public.orders_awaiting_delivery;

*/

