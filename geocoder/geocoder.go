package geocoder

import (
	"auto_delivery/action"
	"auto_delivery/postgresql"
	"auto_delivery/redis"
	rconfig "auto_delivery/redis/redisConfig"
	"ff_system/function"
	"ff_system/logger"
	"fmt"
	"math"
	"strconv"

	"github.com/jmoiron/sqlx"
)

//geocoder - работа с геолокацией
//Определяет примерное время поезздки, сторону света, примерное время поездки
//т.е. общие данные по заказу

//Geocoder - структура базовой информации
type Geocoder struct {
	ID             int64 `db:"id"`
	OrderID        int64 `db:"order_id"`
	Disstance      int64 `db:"disstance"`         //Дистанция
	TravelTime     int64 `db:"travel_time"`       //Предположительное время поездки
	AvrTravelTime  int64 `db:"avr_travel_time"`   //Среднее время поездки согласно истории
	SideOfTheWorld int64 `db:"side_of_the_world"` //Сторона света
}

//NewGeocoder - Создать объект
func NewGeocoder() *Geocoder {
	return new(Geocoder)
}

//DistanceMatrix - структура с данными по координатам и городу
type DistanceMatrix struct {
	X1, Y1 float64 //координаты рестоарана
	X2, Y2 float64 //координаты точки назначения

	City string
}

//GetData - Получим инфу по позаказу с координатами назначения
func (g *Geocoder) GetData(_inOrder action.Orders, _inPoint redis.Point) error {

	// if err := order.Select("OrderID", _inOrderID); err != nil {
	// 	return err
	// }

	plat, err := strconv.ParseFloat(_inPoint.Lat, 64)
	if err != nil {
		return err
	}
	plon, err := strconv.ParseFloat(_inPoint.Lon, 64)
	if err != nil {
		return err
	}
	olat, err := strconv.ParseFloat(_inOrder.Lat, 64)
	if err != nil {
		return err
	}
	olon, err := strconv.ParseFloat(_inOrder.Lon, 64)
	if err != nil {
		return err
	}

	dm := DistanceMatrix{
		X1: plat,
		Y1: plon,
		X2: olat,
		Y2: olon,
	}

	//logger.Messaging.Println("До: ", fmt.Sprintf("%+v", dm))
	dm.deg2rad()
	//logger.Messaging.Println("После:", fmt.Sprintf("%+v", dm))
	g.OrderID = _inOrder.OrderID
	g.Disstance = dm.getDistances()
	g.TravelTime = dm.GetRiding(g.Disstance)
	g.SideOfTheWorld = dm.Azimuth()

	return nil
}

//deg2rad - преобразование в градусы
func (dm *DistanceMatrix) deg2rad() {
	dm.X1 = dm.X1 * (math.Pi / 180)
	dm.Y1 = dm.Y1 * (math.Pi / 180)
	dm.X2 = dm.X2 * (math.Pi / 180)
	dm.Y2 = dm.Y2 * (math.Pi / 180)
}

func (dm *DistanceMatrix) getDistances() int64 {

	paramsCity := rconfig.Get(dm.City)

	logger.Messaging.Println("\n-----\nКоординаты:", dm.X1, dm.X2, dm.Y1, dm.Y2)
	//Найдем первый катет, тут X2 поменяли на X1
	_locaCatetX := dm.formul(dm.X1, dm.X1, dm.Y1, dm.Y2)
	//Найдем второй катет, тут Y2 поменяли на Y1
	_locaCatetY := dm.formul(dm.X1, dm.X2, dm.Y1, dm.Y1)
	//logger.Messaging.Println("_locaCatetX:", _locaCatetX, "\n_locaCatetY:", _locaCatetY)
	//Возьмем больший катет
	_usingStorona := _locaCatetX
	//Теперь получим необходимый нам коэфициент с меньшего катета
	_dopLine := _locaCatetY * paramsCity.Coefficient
	if _locaCatetY > _locaCatetX {
		_usingStorona = _locaCatetY
		_dopLine = _locaCatetX * paramsCity.Coefficient
	}

	//logger.Messaging.Println("Нашли сторону: ", _usingStorona)
	logger.Messaging.Println(fmt.Sprintf("Коэфициент [%f]: %f", paramsCity.Coefficient, _usingStorona+_dopLine))
	return int64(_usingStorona + _dopLine)
}

//Формула вычесления расстояния
func (dm *DistanceMatrix) formul(X1, X2, Y1, Y2 float64) float64 {
	return math.Ceil(6378137 * math.Acos(math.Cos(X1)*math.Cos(X2)*math.Cos(Y1-Y2)+math.Sin(X1)*math.Sin(X2)))
}

//Вычисляем сколько едет
func (dm *DistanceMatrix) GetRiding(_inDistance int64, _inSpeedHour ...float64) int64 {

	paramsCity := rconfig.Get(dm.City)

	_speedMinute := paramsCity.SpeedSec / 60 //Сколько проезжает за минуту
	//logger.Messaging.Println("Проезжает  в минуту: ", _speedMinute)
	_distanceTime := function.Round(float64(_inDistance)/_speedMinute, 0) //Сколько куреьр должен ехать в минутах
	//logger.Messaging.Println("Сколько времени едет (без округления): ", float64(_inDistance)/_speedMinute)
	//logger.Messaging.Println("Сколько времени едет: ", _distanceTime)
	if _distanceTime < 1 {
		_distanceTime = 1
	}
	//logger.Messaging.Println("Результат: ", _distanceTime)

	return int64(_distanceTime)
}

//Azimuth  - вычисляем азимут, вернем ID стороны света
func (dm *DistanceMatrix) Azimuth() int64 {
	//az2 := (dm.X1*dm.X2 + dm.Y1*dm.Y2) / (math.Sqrt(math.Pow(dm.X1, 2)+math.Pow(dm.X2, 2)) * (math.Sqrt(math.Pow(dm.Y1, 2) + math.Pow(dm.Y2, 2))))
	//var az int64 = int64((180 / math.Pi) * math.Atan2(dm.X2-dm.X1, dm.Y2-dm.Y1))

	// dX := dm.X1 - dm.X2
	// dY := dm.Y1 - dm.Y2

	// dist := math.Sqrt((dX * dX) + (dX * dX))
	// dXa := math.Abs(dX)
	// beta := math.Acos(dXa / dist)
	// var angle float64 = 0

	// if dX > 0 {
	// 	if dY < 0 {
	// 		angle = 270 + beta
	// 	} else {
	// 		angle = 270 - beta
	// 	}
	// } else {
	// 	if dY < 0 {
	// 		angle = 90 - beta
	// 	} else {
	// 		angle = 90 + beta
	// 	}
	// }

	//logger.Messaging.Println("\naz2:", az2)

	//az := int64(az2)
	//logger.Messaging.Println("\naz1:", az)

	var fourth int64 = 0
	//Определяем четверти относительно ресторана
	if dm.X2 > dm.X1 && dm.Y2 > dm.Y1 {
		fourth = 1
	} else if dm.X2 < dm.X1 && dm.Y2 > dm.Y1 {
		fourth = 2
	} else if dm.X2 < dm.X1 && dm.Y2 < dm.Y1 {
		fourth = 3
	} else if dm.X2 > dm.X1 && dm.Y2 < dm.Y1 {
		fourth = 4
	}

	logger.Messaging.Println("fourth:", fourth)

	// for key, world := range Sids {
	// 	//logger.Messaging.Println("world.Min :", world.Min, "world.Max :", world.Max)
	// 	if world.Min < az && az <= world.Max {
	// 		//logger.Messaging.Println(world.Name, "\n")
	// 		return key
	// 	}
	// }
	return fourth
}

//SearchSupposed - поиск предположительной группы
func (g *Geocoder) SearchSupposedIdGroups(_inGroupID int64) (groups []int64) {
	switch _inGroupID {
	case 1:
		groups = []int64{4, 1, 2}
	case 2:
		groups = []int64{1, 2, 3}
	case 3:
		groups = []int64{2, 3, 4}
	case 4:
		groups = []int64{3, 4, 1}
	}
	return groups
}

//SearchSupposed - поиск предположительной группы
func (g *Geocoder) CheckSupposedIdGroups(_inGroupID int64, _inPullGroups []int64) bool {

	if len(_inPullGroups) == 0 {
		return false
	}

	for _, group := range _inPullGroups {
		if group == _inGroupID {
			return true
		}
	}

	return false
}

func init() {
	Sids = make(map[int64]SideOfTheWorldData)
	Sids[0] = SideOfTheWorldData{0, 45, "С", "Север"}
	Sids[1] = SideOfTheWorldData{45, 90, "CB", "Северо-Восток"}
	Sids[2] = SideOfTheWorldData{90, 135, "В", "Восток"}
	Sids[3] = SideOfTheWorldData{135, 180, "ЮВ", "Юг-восток"}
	Sids[4] = SideOfTheWorldData{180, 225, "Ю", "Юг"}
	Sids[5] = SideOfTheWorldData{225, 270, "ЮЗ", "Юго-Запад"}
	Sids[6] = SideOfTheWorldData{270, 315, "З", "Запад"}
	Sids[7] = SideOfTheWorldData{315, 360, "СЗ", "Северо-Запад"}
}

type SideOfTheWorldData struct {
	Min          int64
	Max          int64
	Abbreviation string
	Name         string
}

var Sids map[int64]SideOfTheWorldData

// 	 0, //Север
// 	45, //Северо-восток
// 	90, //Восток
// 	135, //Юго-Восток
// 	180, //Юг
// 	225, //Юго-запад
// 	270, //Запад
// 	315, //Северо-запад
// }

// North 	0° 		South 	180°
// North-northeast 	22.5° 	South-southwest 	202.5°
// Northeast 	45° 	Southwest 	225°
// East-northeast 	67.5° 	West-southwest 	247.5°
// East 	90° 	West 	270°
// East-southeast 	112.5° 	West-northwest 	292.5°
// Southeast 	135° 	Northwest 	315°
// South-southeast 	157.5° 	North-northwest 	337.5°

/*
0 (DIRECTION_NORTH) - Игрок смотрит на Север
1 (DIRECTION_NORTH_WEST) - Игрок смотрит на Северо-Запад
2 (DIRECTION_WEST) - Игрок смотрит на Запад
3 (DIRECTION_SOUTH_WEST) - Игрок смотрит на Юго-Запад
4 (DIRECTION_SOUTH) - Игрок смотрит на Юг
5 (DIRECTION_SOUTH_EAST) - Игрок смотрит на Юго-Восток
6 (DIRECTION_EAST) - Игрок смотрит на Восток
7 (DIRECTION_NORTH_EAST) - Игрок смотрит на Северо-Восток

*/

//SetDB - запишем в базу информацию
func (g *Geocoder) SetDB(_inTx *sqlx.Tx) (err error) { //order_id, disstance, travel_time, avr_travel_time, side_of_the_world
	logger.Messaging.Println("SetDB")
	_, err = postgresql.Exec(_inTx, "Insert", "GeocoderOrders", "", g.OrderID, g.Disstance, g.TravelTime, g.AvrTravelTime, g.SideOfTheWorld)

	return err
}

//Select - Получить заказ
func (g *Geocoder) Select(_inOrderID int64) (err error) {
	oo := []Geocoder{}
	if err = postgresql.Select(nil, "Select.GeocoderOrders.OrderID", &oo, _inOrderID); err != nil {
		return
	}
	for _, value := range oo {
		*g = value
		break
	}
	return
}
