package action

//Работа с заказами в модуле

import (
	"auto_delivery/postgresql"
	"encoding/json"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
)

type Orders struct {
	ID           int64           `db:"id"`
	OrderID      int64           `db:"order_id"`
	IdDayPoint   int64           `db:"id_day_point"`
	PointHash    string          `db:"point_hash"`
	StatusID     int64           `db:"status_id"`
	TimeCooked   time.Time       `db:"time_cooked"`
	TimeDelivery time.Time       `db:"time_delivery"`
	Preorder     bool            `db:"preorder"`
	Lat          string          `db:"lat"`
	Lon          string          `db:"lon"`
	UserHash     string          `db:"user_hash"`
	UserName     string          `db:"user_name"`
	Address      json.RawMessage `db:"address"`
	Products     json.RawMessage `db:"products"`

	SideOfTheWorld int64 `db:"side_of_the_world"`
}

func NewOrders() *Orders {
	return new(Orders)
}

//Add - добавление заказа на очередь
func (self *Orders) Add(_inTx *sqlx.Tx) (err error) {

	if self.OrderID == 0 || len(self.PointHash) == 0 /*|| self.StatusID < 4*/ || self.TimeCooked.IsZero() || self.TimeDelivery.IsZero() || len(self.Lat) == 0 || len(self.Lon) == 0 {
		return fmt.Errorf("Один из основных передаваемых параметров пустой %+v", self)
	}

	_, err = postgresql.Exec(_inTx, "Insert", "Orders", "", self.OrderID, self.IdDayPoint, self.PointHash, self.StatusID, self.TimeCooked, self.TimeDelivery, self.Preorder, self.Lat, self.Lon, self.Address, self.Products)

	return err
}

//UpStatus - добавление заказа на очередь
func (self *Orders) UpStatus(_inTx *sqlx.Tx) error {

	if self.OrderID == 0 || self.StatusID == 0 {
		return fmt.Errorf("Один из передаваемых параметров пустой: OrderID [%d], StatusID[%d]", self.OrderID, self.StatusID)
	}

	_, err := postgresql.Exec(_inTx, "Update", "Orders", "Status", self.OrderID, self.StatusID)
	return err
}

//UpStatus - добавление заказа на очередь
func (self *Orders) UpOrder(_inTx *sqlx.Tx) error {

	if self.OrderID == 0 || self.StatusID == 0 || self.TimeCooked.IsZero() {
		return fmt.Errorf("Один из передаваемых параметров пустой: OrderID [%d], StatusID[%d], TimeCooked", self.OrderID, self.StatusID)
	}

	_, err := postgresql.Exec(_inTx, "Update", "Orders", "OrderID", self.OrderID, self.StatusID, self.TimeCooked)
	return err
}

//Delete - удаление заказа с очереди, при отмене
func (self *Orders) Delete() error {
	_, err := postgresql.Exec(nil, "Delete", "Orders", "", self.OrderID)
	return err
}

//Select - Получить заказ
func (self *Orders) Select(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (err error) {
	oo := []Orders{}
	if err = postgresql.Select(_inTx, "Select.Orders."+_inType, &oo, _InArgs...); err != nil {
		return
	}
	for _, value := range oo {
		*self = value
		break
	}
	return
}

//Select - Получить заказ
func (self *Orders) SelectRows(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (oo []Orders, err error) {
	if err = postgresql.Select(_inTx, "Select.Orders."+_inType, &oo, _InArgs...); err != nil {
		return
	}
	return
}
