package action

//Модуль очереди курьров

import (
	"auto_delivery/postgresql"
	"errors"
	"ff_system/logger"
	"fmt"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
)

type Courier struct {
	ID           int64  `db:"id"`
	UserHash     string `db:"user_hash"`
	UserName     string `db:"user_name"`
	SurName      string `db:"sur_name"`
	FirstName    string `db:"first_name"`
	SecondName   string `db:"second_name"`
	Phone        string `db:"phone"`
	PointHash    string `db:"point_hash"`
	Prioritys    int64  `db:"prioritys"`
	Free         bool   `db:"free"`
	PackOrders   int64  `db:"pack_orders"`
	InTenStatus  bool   `db:"in_ten_status"`
	AutoDelivery bool   `db:"auto_delivery"`

	Restaurant bool //Показывает что нужно добавить курьера или убрать?
}

type CouriersPackOrders struct {
	ID         int64     `db:"id"`
	UserHash   string    `db:"user_hash"`
	OrderID    int64     `db:"order_id"`
	IdDayPoint int64     `db:"id_day_point"`
	StatusID   int64     `db:"status_id"`
	TimeNow    time.Time `db:"time_now"`
	Active     bool      `db:"active"`
	Products   string    `db:"products"`
	Address    string    `db:"address"`
	Priority   int64     `db:"priority"`
}

//Couriers - общий объект для работы с курьерами
var Couriers *Courier

//CouriersMutex - мьютекс разделенный по точкам
type courierMutex struct {
	mutex sync.Map
}

var CouriersMutex courierMutex

func (cm *courierMutex) Set(point_hash string) {
	_, ok := CouriersMutex.mutex.Load(point_hash)
	//Запись есть
	if ok {
		return
	}
	var rwm *sync.RWMutex
	rwm = &sync.RWMutex{}
	CouriersMutex.mutex.Store(point_hash, rwm)
}

func (cm *courierMutex) Delete(point_hash string) {
	_, ok := CouriersMutex.mutex.Load(point_hash)
	if ok {
		CouriersMutex.mutex.Delete(point_hash)
	}
}

func (cm *courierMutex) Lock(point_hash string) error {
	rwm, err := CouriersMutex.get(point_hash)
	if err != nil {
		return err
	}
	rwm.Lock()
	return nil
}

func (cm *courierMutex) Unlock(point_hash string) error {
	rwm, err := CouriersMutex.get(point_hash)
	if err != nil {
		return err
	}
	rwm.Unlock()
	return nil
}

func (cm *courierMutex) RLock(point_hash string) error {
	rwm, err := CouriersMutex.get(point_hash)
	if err != nil {
		return err
	}
	rwm.RLock()
	return nil
}

func (cm *courierMutex) RUnlock(point_hash string) error {
	rwm, err := CouriersMutex.get(point_hash)
	if err != nil {
		return err
	}
	rwm.RUnlock()
	return nil
}

//get - получение мьютекса с преобразованием типа
func (cm *courierMutex) get(point_hash string) (*sync.RWMutex, error) {
	rwmi, ok := CouriersMutex.mutex.Load(point_hash)
	if !ok {
		return nil, errors.New("Битый мьютекс: [" + point_hash + "]")
	}

	rwm, ok := rwmi.(*sync.RWMutex)
	if !ok {
		return nil, errors.New("Битый тип мьютекса: [" + point_hash + "][" + fmt.Sprintf("%T", rwmi) + "]")
	}
	return rwm, nil
}

//--------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------//
//------------------------------------	Couriers	------------------------------------//
//--------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------//
//--------------------------------------------------------------------------------------//

//NewCouriers - генерация объекта для работы с очередью
func NewCouriers() *Courier {
	return new(Courier)
}

//Select - Получить информацию по курьеру
func (с *Courier) Select(_inType string, _InArgs ...interface{}) (err error) {
	cc := []Courier{}
	if err = postgresql.Select(nil, "Select.Couriers."+_inType, &cc, _InArgs...); err != nil {

		return
	}
	for _, value := range cc {
		*с = value
		break
	}
	return
}

//Select - Получить информацию по курьеру
func (с *Courier) SelectAll(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (arrCourier []Courier, err error) {
	if err = postgresql.Select(_inTx, "Select.Couriers."+_inType, &arrCourier, _InArgs...); err != nil {
		return
	}
	return
}

//Добавление курьера в очередь при отктрытии смены
func (c *Courier) Set() error {
	_, err := postgresql.Exec(nil, "Insert", "Couriers", "", c.UserHash, c.UserName, c.SurName, c.FirstName, c.SecondName, c.Phone, c.PointHash)
	return err
}

//Удаление курьера из очереди
func (c *Courier) Delete() error {
	_, err := postgresql.Exec(nil, "Delete", "Couriers", "UserHash", c.UserHash)
	return err
}

//Удаление курьера из очереди
func (c *Courier) Update(_inTx *sqlx.Tx, _inType string) error {
	_, err := postgresql.Exec(nil, "Update", "Couriers", _inType, c.UserHash, c.AutoDelivery)
	return err
}

//UpdatePrioritys - обновление порядкого номера в очереди, подтверждение что готов
func (c *Courier) UpdatePrioritys() error {
	CouriersMutex.Lock(c.PointHash)
	defer CouriersMutex.Unlock(c.PointHash)

	logger.Application.Println("c.Restaurant:", c.Restaurant)
	restor := c.Restaurant
	//Получим информаю по курьеру
	err := c.Select("UserHash", c.UserHash)
	if err != nil {
		return err
	}

	if len(c.Phone) <= 0 {
		return errors.New("Данный курьер не в системе")
	}

	logger.Application.Println("!c.Free:", !c.Free)
	logger.Application.Println("c.Prioritys:", c.Prioritys)
	logger.Application.Println("c.PackOrders:", c.PackOrders)
	logger.Application.Println("c.Restaurant:", restor)
	if restor {

		if !c.Free && c.Prioritys == 0 && c.PackOrders == 0 {
			_, err = postgresql.Exec(nil, "Update", "Couriers", "Prioritys", c.UserHash)
		} else {
			return fmt.Errorf("Курьер уже в системе параментры: [Restaurant:true] [Free:%t] [Prioritys:%d] [PackOrders:%d]", c.Free, c.Prioritys, c.PackOrders)
		}
	} else {
		//Если курьера нужно скинуть с очереди
		if c.Free && c.Prioritys != 0 && c.PackOrders == 0 {
			_, err = postgresql.Exec(nil, "Update", "Couriers", "PackOrders", c.UserHash, 0)
		} else {
			return fmt.Errorf("Курьер уже в системе параментры: [Restaurant:false] [Free:%t] [Prioritys:%d] [PackOrders:%d]", c.Free, c.Prioritys, c.PackOrders)
		}
	}
	return err
}

func (c *Courier) UpdatePrioritysNoValidation(_inTx *sqlx.Tx, _inUserHash string) error {
	_, err := postgresql.Exec(_inTx, "Update", "Couriers", "Prioritys", _inUserHash)
	return err
}

// //UpdatePackOrders - обновление пака заказов
// func (c *Courier) UpdatePackOrders(tx *sqlx.Tx, Prioritys int64) error {
// 	CouriersMutex.Lock(c.PointHash)
// 	defer CouriersMutex.Unlock(c.PointHash)

// 	_, err := postgresql.Exec(nil, "Update", "Couriers", "Prioritys", c.Prioritys)
// 	return err
// }

//SetOrders - устанавливает пачку заказов курьеру
//::TODO
func (c *Courier) SetOrders(_inTx *sqlx.Tx, _inUserHash string, _inOrders []int64) error {

	if _inTx == nil {
		return errors.New("Функция Courier.SetOrders() поддерживает работу только с открытой транзакцией")
	}

	CouriersMutex.Lock(c.PointHash)
	defer CouriersMutex.Unlock(c.PointHash)

	//Получим информаю по курьеру
	if err := c.Select("UserHash", _inUserHash); err != nil {
		return err
	}

	logger.Messaging.Println(fmt.Sprintf("%+v", c))

	if !c.Free {
		return errors.New("Курьер [" + c.UserHash + "] уже занят или не на ресторане")
	}

	if len(c.UserHash) <= 0 {
		return errors.New("Не указан Hash пользователя")
	}

	packOrdersID, err := postgresql.TxExecReturnID(_inTx, "Insert", "CouriersPackOrders", "", c.UserHash)
	if err != nil {
		return err
	}

	err = postgresql.TxExec(_inTx, "Update", "Couriers", "PackOrders", c.UserHash, packOrdersID)
	if err != nil {
		return err
	}

	for _, order := range _inOrders {
		err := postgresql.TxExec(_inTx, "Insert", "CouriersOrders", "", packOrdersID, order)
		if err != nil {
			return err
		}
	}

	return nil
}

//SetOrderDelivered - Когда заказ доставлен
func (c *Courier) SetOrderDelivered(_inTx *sqlx.Tx, OrderID int64) error {
	CouriersMutex.Lock(c.PointHash)
	defer CouriersMutex.Unlock(c.PointHash)

	_, err := postgresql.Exec(_inTx, "Update", "CouriersOrders", "Active", OrderID)
	if err != nil {
		return err
	}

	return nil
}

func (c *Courier) Check(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) (bool, error) {
	CouriersMutex.Lock(c.PointHash)
	defer CouriersMutex.Unlock(c.PointHash)

	return postgresql.Check(_inTx, "Check.CouriersOrders."+_inType, _InArg...)
}

//::TODO
func (cpo *CouriersPackOrders) Query(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (cpoarray []CouriersPackOrders, err error) {
	if err = postgresql.Select(nil, "Select.CouriersPackOrders."+_inType, &cpoarray, _InArgs...); err != nil {
		return
	}
	return
}

func (cpo *CouriersPackOrders) Delete(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (err error) {
	_, err = postgresql.Exec(nil, "Delete", "CouriersPackOrders", _inType, _InArgs...)
	return
}

func (cpo *CouriersPackOrders) Rollback(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (err error) {
	_, err = postgresql.Exec(nil, "Rollback", "CouriersOrders", _inType, _InArgs...)
	return
}
