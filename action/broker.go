package action

import (
	"auto_delivery/config"
	"encoding/json"
	"ff_system/connect/structExchange"
	"ff_system/rabbitmq"
)

var Broker rabbitmq.Publisher

func InitStock() {
	err := Broker.Init(config.Config.BROKER.RabbitLogin, config.Config.BROKER.RabbitPassword, "autoDelivery")
	if err != nil {
		panic(err)
	}
}

type WebInfoAutoDelivery struct {
	PointHash string
	Message   json.RawMessage
}

//SendEventCourier - отправка уведомлений об изменении очереди курьеров
func SendEventCourier(_inPointHash string, _inMessage interface{}) error {

	mb, err := json.Marshal(_inMessage)
	if err != nil {
		return err
	}

	return Broker.Send("autoDelivery", &structExchange.WMessage{
		Query: "Update",
		Tables: []structExchange.WTable{
			{
				Name:          "UpdateCourier",
				TypeParameter: "",
				Values: WebInfoAutoDelivery{
					_inPointHash,
					mb,
				},
			},
		},
		PointHash: _inPointHash,
	})

}
