package action

import (
	"auto_delivery/postgresql"
	"sync"
	"time"

	"github.com/jmoiron/sqlx"
)

//orders_awaiting_delivery - формирование заказов ожидающих доставку так как нет курьера или еще не готовы.

type OrdersAwaitingDelivery struct {
	ID             int64     `db:"id"`
	OrderID        int64     `db:"order_id"` //Вспомогательное поле
	IdDayPoint     int64     `db:"id_day_point"`
	StatusID       int64     `db:"status_id"`
	TimeCooked     time.Time `db:"time_cooked"`
	TimeDelivery   time.Time `db:"time_delivery"`
	PointHash      string    `db:"point_hash"`
	Pack           int64     `db:"pack"`
	TimeInsert     time.Time `db:"time_insert"`
	SideOfTheWorld int64     `db:"side_of_the_world"` //Сторона света
	Disstance      int64     `db:"disstance"`         //Дистанция
	TravelTime     int64     `db:"travel_time"`       //Предположительное время поездки
	Lat            string    `db:"lat"`
	Lon            string    `db:"lon"`
}

type OrdersAwaitingDeliveryList struct {
	ID      int64 `db:"id"`
	PackID  int64 `db:"pack_id"`
	OrderID int64 `db:"order_id"`
}

var mutexOAD *sync.RWMutex

func init() {
	mutexOAD = &sync.RWMutex{}
}

func NewOrdersAwaitingDelivery() *OrdersAwaitingDelivery {
	return new(OrdersAwaitingDelivery)
}

func NewOrdersAwaitingDeliveryList() *OrdersAwaitingDeliveryList {
	return new(OrdersAwaitingDeliveryList)
}

func (oad *OrdersAwaitingDelivery) SetDB(_inTx *sqlx.Tx) error {
	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	_, err := postgresql.Exec(_inTx, "Insert", "OrdersAwaitingDelivery", "", oad.PointHash, oad.Pack, oad.TimeInsert)

	return err
}

func (oad *OrdersAwaitingDelivery) Delete(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) error {
	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	err := postgresql.TxExec(_inTx, "Delete", "OrdersAwaitingDelivery", _inType, _InArg...)
	return err
}

func (oad *OrdersAwaitingDelivery) SelectLastPackID() (pack int64, err error) {

	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	err = postgresql.Get("Select.OrdersAwaitingDelivery.LastPack", oad)
	return oad.Pack, err

	// oo := []OrdersAwaitingDelivery{}
	// if err = postgresql.Select("Select.OrdersAwaitingDelivery."+_inType, &oo, _InArgs...); err != nil {
	// 	return
	// }
	// for _, value := range oo {
	// 	*oad = value
	// 	break
	// }
	//return
}

func (oad *OrdersAwaitingDelivery) Query(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) (oadArr []OrdersAwaitingDelivery, err error) {

	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	err = postgresql.Select(_inTx, "Select.OrdersAwaitingDelivery."+_inType, &oadArr, _InArg...)

	// rows, err := postgresql.Query(_inTx, "Select.OrdersAwaitingDelivery."+_inType, _InArg...)
	// if err != nil {
	// 	return
	// }
	// defer rows.Close()
	// for rows.Next() {
	// 	locOAD := OrdersAwaitingDelivery{}
	// 	if err := rows.Scan(&locOAD.OrderID, &locOAD.Pack); err != nil {
	// 		return nil, err
	// 	}
	// 	oadArr = append(oadArr, locOAD)
	// }

	return
}

//CheckUsingOrderID - Чекает существует ли заказ
func (oad *OrdersAwaitingDelivery) Check(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) (bool, error) {

	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	return postgresql.Check(_inTx, "Check.OrdersAwaitingDeliveryList."+_inType, _InArg...)
}

func (oadl *OrdersAwaitingDeliveryList) SetDB(_inTx *sqlx.Tx) error {
	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	_, err := postgresql.Exec(_inTx, "Insert", "OrdersAwaitingDeliveryList", "", oadl.PackID, oadl.OrderID)
	return err
}

func (oad *OrdersAwaitingDeliveryList) Query(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) (oadArr []OrdersAwaitingDeliveryList, err error) {

	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	err = postgresql.Select(_inTx, "Select.OrdersAwaitingDeliveryList."+_inType, &oadArr, _InArg...)

	return
}

func (oad *OrdersAwaitingDeliveryList) Delete(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) error {
	mutexOAD.Lock()
	defer mutexOAD.Unlock()

	err := postgresql.TxExec(_inTx, "Delete", "OrdersAwaitingDeliveryList", _inType, _InArg...)
	return err
}
