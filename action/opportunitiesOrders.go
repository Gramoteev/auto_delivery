package action

import (
	"auto_delivery/postgresql"
	"fmt"
	"math"
	"time"

	"ff_system/logger"

	"github.com/jmoiron/sqlx"
)

type GeocoderDubl struct {
	ID        int64 `db:"id"`
	OrderID   int64 `db:"order_id"`
	Disstance int64 `db: "distance"` //Дистанция
}

//opportunitiesOrders - опрелеляет варианты для заказа
//Возможности заказов
type opportunitiesOrders struct {
	Priority    map[int64]OrderGroupOpportunitiesList // Заказы которые находятся в приоритете группировки по стороне света  //1
	Radius      map[int64]OrderGroupOpportunitiesList // Которые находятся в небольшом отдалении от точки                    //2
	LowPriority map[int64]OrderGroupOpportunitiesList // Заказы которые находятся в приоритете группировки по стороне света  //3
}

type OrderGroupOpportunitiesList struct {
	ID                        int64     `db:"id"`
	OrderGroupOpportunitiesID int64     `db:"order_group_opportunities_id"`
	Priority                  int64     `db:"priority"`
	OrderID                   int64     `db:"order_id"`
	StatusID                  int64     `db:"status_id"`
	Lat                       string    `db:"lat"`
	Lon                       string    `db:"lon"`
	TimeCooked                time.Time `db:"time_cooked"`
	TimeDelivery              time.Time `db:"time_delivery"`
	Preorder                  bool      `db:"preorder"`
	Disstance                 int64     `db:"disstance"`         //Дистанция
	TravelTime                int64     `db:"travel_time"`       //Предположительное время поездки
	SideOfTheWorld            int64     `db:"side_of_the_world"` //Сторона света
	DisstanceBasePoint        int64     `db:"disstance_base_point"`
	TravelTimeBasePoint       int64     `db:"travel_time_base_point"`
}

//NewOpportunitiesOrders - вернет объект структуры
func NewOpportunitiesOrders() *opportunitiesOrders {
	op := new(opportunitiesOrders)
	op.Priority = make(map[int64]OrderGroupOpportunitiesList)
	op.Radius = make(map[int64]OrderGroupOpportunitiesList)
	op.LowPriority = make(map[int64]OrderGroupOpportunitiesList)
	return op
}

// func (op *opportunitiesOrders) SetDB(_inOrder int64) error {
// 	tx, err := postgresql.Begin()
// 	if err != nil {
// 		return err
// 	}
// 	defer tx.Rollback()

// 	logger.Messaging.Println("Вставляем OrderGroupOpportunities:", _inOrder)
// 	id, err := postgresql.TxExecReturnID(tx, "Insert", "OrderGroupOpportunities", "", _inOrder)
// 	if err != nil {
// 		return err
// 	}

// 	logger.Messaging.Println("Получили ID:", id)

// 	for _, value := range op.Priority {
// 		logger.Messaging.Println("Вствялем Priority OrderGroupOpportunitiesList id [", id, "]:", fmt.Sprintf("%+v", value))
// 		//order_group_opportunities_id, order_id, priority, travel_time, avr_travel_time, side_of_the_world
// 		err := postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "", id, value.OrderID, 1, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 		//Инвертируем на зеркальный заказ, ему так же выставляем приоритеты
// 		logger.Messaging.Println("Вствялем Priority OrderGroupOpportunitiesList.OrderIDGroup value.OrderID, _inOrder [", value.OrderID, ",", _inOrder, "]:", fmt.Sprintf("%+v", value))
// 		err = postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "OrderIDGroup", value.OrderID, _inOrder, 1, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	for _, value := range op.Radius {
// 		logger.Messaging.Println("Вствялем Radius OrderGroupOpportunitiesList id [", id, "]:", fmt.Sprintf("%+v", value))
// 		//order_group_opportunities_id, priority, travel_time, avr_travel_time, side_of_the_world
// 		err := postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "", id, value.OrderID, 2, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 		logger.Messaging.Println("Вствялем Radius OrderGroupOpportunitiesList.OrderIDGroup value.OrderID, _inOrder [", value.OrderID, ",", _inOrder, "]:", fmt.Sprintf("%+v", value))
// 		err = postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "OrderIDGroup", value.OrderID, _inOrder, 2, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	for _, value := range op.LowPriority {
// 		logger.Messaging.Println("Вствялем LowPriority OrderGroupOpportunitiesList id [", id, "]:", fmt.Sprintf("%+v", value))
// 		//order_group_opportunities_id, priority, travel_time, avr_travel_time, side_of_the_world
// 		err := postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "", id, value.OrderID, 3, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 		logger.Messaging.Println("Вствялем LowPriority OrderGroupOpportunitiesList.OrderIDGroup value.OrderID, _inOrder [", value.OrderID, ",", _inOrder, "]:", fmt.Sprintf("%+v", value))
// 		err = postgresql.TxExec(tx, "Insert", "OrderGroupOpportunitiesList", "OrderIDGroup", value.OrderID, _inOrder, 3, value.Disstance, value.TravelTime, value.SideOfTheWorld)
// 		if err != nil {
// 			return err
// 		}
// 	}

// 	return tx.Commit()
// }

func (op *opportunitiesOrders) SetDB(_inTx *sqlx.Tx, _inOrder int64) (int64, error) {
	logger.Messaging.Println("Вставляем SetDB opportunitiesOrders:", _inOrder)
	idInterface, err := postgresql.TxExecReturnID(_inTx, "Insert", "OrderGroupOpportunities", "", _inOrder)
	if err != nil {
		return 0, err
	}

	id, ok := idInterface.(int64)
	if !ok {
		return 0, fmt.Errorf("Оишбка приобразования %+v к int64", idInterface)
	}

	return id, nil
}

func (op *opportunitiesOrders) GetDB(_inType string, _InArgs ...interface{}) (ogol []OrderGroupOpportunitiesList, err error) {
	if err = postgresql.Select(nil, "Select.OrderGroupOpportunitiesList."+_inType, &ogol, _InArgs...); err != nil {
		return
	}
	return
}

func (op *opportunitiesOrders) Delete(_inTx *sqlx.Tx, _inType string, _InArgs ...interface{}) (err error) {
	_, err = postgresql.Exec(_inTx, "Delete", "OrderGroupOpportunities", _inType, _InArgs...)
	if err != nil {
		return
	}
	return
}

//CheckUsingOrderID - Чекает существует ли заказ
func (oad *opportunitiesOrders) Check(_inTx *sqlx.Tx, _inType string, _InArg ...interface{}) (bool, error) {
	return postgresql.Check(_inTx, "Check.OrderGroupOpportunities."+_inType, _InArg...)
}

func (op *OrderGroupOpportunitiesList) SetDB(_inTx *sqlx.Tx, _nIDParentOpportunities int64, _inOrderIDOpportunities int64, _inPriority int64) error {

	logger.Messaging.Println("Вставляем:", _nIDParentOpportunities, ":- ", _inOrderIDOpportunities, ":- ", _inPriority)
	if _nIDParentOpportunities <= 0 || _inOrderIDOpportunities <= 0 || _inPriority <= 0 {
		return fmt.Errorf("Один из параметров пустой в функции (OrderGroupOpportunitiesList.SetDB()): _nIDParentOpportunities [%d], _inOrderIDOpportunities [%d], _inPriority [%d]", _nIDParentOpportunities, _inOrderIDOpportunities, _inPriority)
	}

	logger.Messaging.Println("Вставляем:", _inOrderIDOpportunities)
	err := postgresql.TxExec(_inTx, "Insert", "OrderGroupOpportunitiesList", "", _nIDParentOpportunities, op.OrderID, _inPriority, op.Disstance, op.TravelTime, op.SideOfTheWorld)
	if err != nil {
		return err
	}
	//Инвертируем на зеркальный заказ, ему так же выставляем приоритеты
	logger.Messaging.Println("Вствялем value.OrderID, _inOrder [", op.OrderID, ",", _inOrderIDOpportunities, "]:", fmt.Sprintf("%+v", op))
	err = postgresql.TxExec(_inTx, "Insert", "OrderGroupOpportunitiesList", "OrderIDGroup", op.OrderID, _inOrderIDOpportunities, _inPriority, op.Disstance, op.TravelTime, op.SideOfTheWorld)
	if err != nil {
		return err
	}
	return nil
}

func (op *opportunitiesOrders) Sort(ogol []OrderGroupOpportunitiesList) {
	for i := 0; i < len(ogol); i++ {
		for j := i; j < len(ogol); j++ {
			abs := math.Abs(float64(ogol[i].Disstance - ogol[j].Disstance))
			if ogol[i].StatusID != 8 && ogol[j].StatusID == 8 && 200 < abs && abs < 1000 {
				swap(ogol, i, j)
			} else {
				if 200 < abs && abs < 1000 && ogol[j].TimeCooked.Before(ogol[i].TimeCooked) {
					swap(ogol, i, j)
				}
			}
		}
	}
	return
}

func swap(ar []OrderGroupOpportunitiesList, i, j int) {
	tmp := ar[i]
	ar[i] = ar[j]
	ar[j] = tmp
}
